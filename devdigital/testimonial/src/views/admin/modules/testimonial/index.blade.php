@extends('admin.layouts.app')
@section('content')
<div class="row mx-0 mb-3">
    <div class="col-6">
        <h1 class="page-title"><?php echo __('testimonial::testimonial.pagetitle'); ?></h1>
    </div>
    <div class="top-btn-box col-6" id="normal_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a tabindex="1" href="javascript:void(0)" class="btn search-btn btn-sm show hide btn-primary mr-1"
                id="search-btn">
                <i class="icon-search-icon top-icon"></i>
                <span class="btn-title">{{ __('testimonial::common.search') }}</span>
            </a>
            <a tabindex="2" href="{{route('testimonial.create')}}" class="btn addnew-btn btn-sm btn-primary"
                id="add-btn">
                <i class="icon-addnew top-icon"></i>
                <span class="btn-title">{{ __('testimonial::common.add') }}</span>
            </a>
        </div>
    </div>
    <div class="top-btn-box hide col-6" id="action_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a href="javascript:void(0)" class="btn btn-sm active-btn btn-primary mr-1"
                onclick="submitactionform('active');">
                <i class="icon-radiobutton_checked top-icon"></i>
                <span class="btn-title">{{ __('testimonial::common.active') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm inactive-btn btn-primary mr-1"
                onclick="submitactionform('inactive');">
                <i class="icon-radio_button_unchecked top-icon"></i>
                <span class="btn-title">{{ __('testimonial::common.inactive') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm delete-btn btn-dark" onclick="submitactionform('delete');">
                <i class="icon-close-icon top-icon"></i>
                <span class="btn-title">{{ __('testimonial::common.delete') }}</span>
            </a>
        </div>
    </div>
</div>
<div class="col-12 admin-holder">
<div class="row">
        <div class="{{ Request::has('status') && Request::has('search') ? 'show' : 'hide' }}" id="searchbox">
            <form name="frmsearch" id="frmsearch" action="{{ route('testimonial.index') }}" method="GET" class="col-12">
                @foreach (Request::all() as $key=>$value)
                @if (in_array($key,['search','status','btnsearch']))
                @continue
                @else
                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endif
                @endforeach
                <div class="row">
                    <div class="form-group col-md-6 col-12">
                        <label>{{ __('testimonial::testimonial.client_name') }}/{{ __('testimonial::testimonial.description') }}</label>
                        <input tabindex="3" name="search" id="search"
                            placeholder="{{ __('testimonial::common.search') ." ". __('testimonial::testimonial.pagetitle') }}" type="text"
                            class="form-control" value="{{ Request::get('search') }}">
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-5 col-12">
                        <label>{{ __('testimonial::testimonial.status') }}</label>
                        <select tabindex="4" name="status" id="status" class="form-control">
                            <option value="">{{ __('testimonial::common.select_status') }}</option>
                            @foreach (config('status') as $value => $label)
                            <option value="{{$value}}"
                                {{ Request::get('status') != "" && intval(Request::get('status')) === $value ? 'selected' : '' }}>
                                {{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <button tabindex="5" type="submit" class="btn submit-btn btn-primary mr-1" id="btnsearch"
                            name="btnsearch">{{ __('testimonial::common.search') }}</button>
                        <a href="{{ route('testimonial.index', ['search' => '', 'status' => '']) }}"
                            class="btn reset-btn btn-primary mr-1">{{ __('testimonial::common.reset') }}</a>
                        <button tabindex="7" type="button" class="btn close-btn btn-dark"
                            id="search-btn-h">{{ __('testimonial::common.close') }}</button>
                        <hr>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 sortable-main">
            <section id="wrapper">
                <form name="frmlist" id="frmlist" action="{{ route('testimonial.bulkaction') }}" method="POST">
                    @csrf
                    <input type="hidden" name="bulk-action" value="">
                    <table data-orders="6" data-target="3" class="sort_table table table-hover mb-0" width="100%"
                        defaultdir="desc" data-sort-url="{{ route('testimonial.sort') }}">
                        <thead>
                            <tr>
                                <th class="hide" scope="col">

                                </th>
                                <th class="active-box" scope="col">
                                    <i class="sort"></i>
                                </th>
                                <th class="check-box nosort" scope="col">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input" name="selectAll"
                                            id="selectAll" onclick="checkAll();">
                                        <label class="custom-control-label" for="selectAll">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col" class="control nosort">
                                    <!-- this blank column is responsive controll -->
                                </th>
                                <th scope="col">
                                    <span>{{ __('testimonial::testimonial.client_name') }}</span>
                                </th>
                                <th scope="col">
                                    <span>{{ __('testimonial::testimonial.update_at') }}</span>
                                </th>
                                <th scope="col">
                                    <span>{{ __('testimonial::testimonial.display_order') }}</span>
                                </th>
                                <th class="text-right nosort" scope="col">{{__('testimonial::common.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($testimonials as $testimonial)
                            <tr id="row_id_{{$testimonial->id}}">
                                <td class="hide">{{$testimonial->id}}</td>
                                <td class="active-box ">
                                    <i style="display:none">{{ $testimonial->status }}</i>
                                    <a href="{{ route('testimonial.changestatus', ['id' => $testimonial->id]) }} ">
                                        <span
                                            class="sort {{ $testimonial->status == 1 ? 'active' : 'inactive' }} "></span></a>
                                </td>
                                <td class="check-box ">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input chkbox action-checkbox"
                                            id="filled-in-box_{{ $testimonial->id }}" name="id[]"
                                            value="{{$testimonial->id}}">
                                        <label class="custom-control-label"
                                            for="filled-in-box_{{ $testimonial->id }}">&nbsp;</label>
                                    </div>
                                </td>
                                <td></td>
                                <td class="">
                                    <a href="{{ route('testimonial.edit', ['id' => $testimonial->id]) }}"> {!!
                                        nl2br($testimonial->client_name) !!}</a>
                                </td>
                                <td id="update_id_{{$testimonial->id}}" class="update-data"
                                    data-sort="{{ \Carbon\Carbon::parse( $testimonial->updated_at)->format('Y/m/d H:i:s')}}">
                                    {{ $testimonial->updated_at }}
                                </td>
                                <td class="row_sorting">
                                    {{ $testimonial->display_order }}
                                </td>
                                <td class="text-right ">
                                    <a href="{{ route('testimonial.edit', ['id' => $testimonial->id]) }}"><i
                                            class="icon-edit-icon"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @if (count($testimonials) == 0)
                            <tr class="noreocrd nosort">
                                <td colspan="9" class="text-center">
                                    {{ __('testimonial::testimonial.no_result') }}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </form>
            </section>
        </div>
    </div>
</div>
@endsection
