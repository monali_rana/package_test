@extends('admin.layouts.app')
@section('content')
<form method="post"
    action="{{ (isset($testimonial->id) ? route('testimonial.update',$testimonial->id) : route('testimonial.store'))  }}"
    name="frmaddedit" id="frmaddedit">
    <div class="row mx-0 mb-3">
        <div class="col-lg-6">
            <h1 class="page-title">
                {{ (isset($testimonial->id) ? __('testimonial::testimonial.editpagetitle') : __('testimonial::testimonial.addpagetitle'))  }}</h1>
        </div>
        <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center align-items-center">
            <div class="top-btn-box">
                <div class="top-btn-box d-flex">
                    <a tabindex="5" href="{{ route('testimonial.index') }}" class="btn btn-sm btn-dark mr-1"><i
                            class="icon-close-icon top-icon"></i> <span>{{ __('testimonial::common.cancel') }}</span></a>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='save'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('testimonial::common.save') }}</span></button>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary" id="btnsave" name="btnsave"
                        value='savecontinue'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('testimonial::common.savecontinue') }}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @csrf
        @if(isset($testimonial->id)) @method('PUT') @endif
        <div class="row">
            <div class="form-group col-md-6">
                <label for="client_name">{{ __('testimonial::testimonial.client_name') }}<span class="text-danger">*</span></label>
                <input id="client_name" type="text" class="form-control @error('client_name') is-invalid @enderror"
                    name="client_name" value="{{old('client_name',$testimonial->client_name)}}"
                    data-validator-label="author/client_name" data-validator="required">
                <div class="errormessage">@error('client_name') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                <label for="url">{{ __('testimonial::testimonial.url') }}</label>
                <input pattern="^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$" id="url" type="url" class="form-control @error('url') is-invalid @enderror" name="url"
                    value="{{old('url',$testimonial->url)}}" data-validator="url">
                <small class="text-mute">{{ __('testimonial::testimonial.url') }} must contain <b>http://</b> or
                    <b>https://</b></small>
                <div class="errormessage">@error('url') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                @component('admin.component.status_dropdown',['refModel'=>'testimonial','status'=>
                $testimonial->status,'required'=>false,'default'=>1] )
                @endcomponent
            </div>
            <div class="form-group col-md-6">
                <label for="display_order">{{ __('testimonial::testimonial.display_order') }}</label>
                @if(isset($testimonial->id))
                <select class="form-control " id="display_order" name="display_order">
                    @for ($i = 1; $i <= $maxnumber; $i++) <option value="{{ $i }}" @if( old('display_order',
                        $testimonial->display_order) == $i ) selected @endif>{{ $i }}</option>
                        @endfor
                </select>
                @else
                <input id="display_order" type="text" class="form-control @error('display_order') is-invalid @enderror"
                    name="display_order" value="{{$maxnumber + 1 }}" readonly>
                @endif

                <div class="errormessage">@error('display_order') {{  $message }} @enderror</div>

            </div>
            @if(siteconfig('field_testimonial_title_display') == 'Yes')
            <div class="form-group col-md-6">
                <label for="temp_image">{{ __('testimonial::testimonial.image') }}</label>
                <input type="file" data-size="{{ getImageUploadSizeInMB('testimonial') }}" class="dropify image-upload"
                    name="temp_image" data-folder="testimonial"
                    {{ old('image',$testimonial->image) != "" ? 'data-default-file='.getImageUrl(old('image',$testimonial->image),'testimonial') : ''}} />

                <div><small>{{ getImageRecommendedSize('testimonial') }}</small></div>

                <input name="image" type="hidden" value="{{old('image',$testimonial->image)}}" />
                <div id="error_image" class="errormessage  @error('image') @else{{  'd-none'  }} @enderror">
                    @error('image'){{  $message }}@enderror</div>

            </div>

            <div class="form-group col-md-6">
                <label for="image_alt">{{ __('testimonial::testimonial.image_alt') }}</label>
                <input id="image_alt" type="text" class="form-control @error('image_alt') is-invalid @enderror"
                    name="image_alt" value="{{old('image_alt',$testimonial->image_alt)}}">
                <div class="errormessage">@error('image_alt') {{  $message }} @enderror</div>

            </div>
            @endif  
            <div class="form-group col-md-12">
                <label for="description">{{ __('testimonial::testimonial.description') }} <span class="text-danger">*</span></label>
                <textarea id="description" class="tinymce form-control @error('description') is-invalid @enderror"
                    data-validator="required" name="description"
                    rows="15">{{old('description',$testimonial->description)}}</textarea>
                <div class="errormessage">@error('description'){{  $message }} @enderror</div>
            </div>


        </div>
    </div>
</form>
@endsection
