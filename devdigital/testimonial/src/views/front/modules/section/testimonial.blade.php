<section class="testimonial-holder">
<div id="testimonialslider" class="owl-carousel owl-theme">
    @foreach ($testimonials as $testimoial)
        <div class="item">
            <div class="container">
            <div class="row h-100">
                <div class="col-md-5 col-lg-5 col-xl-4 p-inherit">
                    <div class="slideImage">
                    @if($testimoial->url)
                        <a href="{{ $testimoial->url }}">
                        <picture>
                            <source media="(max-width: 980px)" srcset="{{ getImageURL($testimoial->image, 'testimonial', 'fit-270x380') }}">
                            <source media="(max-width: 1366px)" srcset="{{ getImageURL($testimoial->image, 'testimonial', 'fit-420x600') }}">
                            <img src="{{ getImageURL($testimoial->image, 'testimonial', 'fit-420x600') }}" class="img-fluid" alt="{{ $testimoial->image_alt }}">
                        </picture>
                        </a>
                        @else
                        <picture>
                            <source media="(max-width: 980px)" srcset="{{ getImageURL($testimoial->image, 'testimonial', 'fit-270x380') }}">
                            <source media="(max-width: 1366px)" srcset="{{ getImageURL($testimoial->image, 'testimonial', 'fit-420x600') }}">
                            <img src="{{ getImageURL($testimoial->image, 'testimonial', 'fit-420x600') }}" class="img-fluid" alt="{{ $testimoial->image_alt }}">
                        </picture>
                        @endif
                    </div>
                </div>
                <div class="col-md-7 col-lg-7 col-xl-8 d-flex align-items-center">
                <div class="slideContent">
                    <h3>{{ $testimoial->client_name }}</h3>
                    <div class="clearfix"></div>
                    <span>{!! nl2br($testimoial->sub_title) !!}</span>
                    {!! $testimoial->description !!}
                </div>
                </div>
            </div>
            </div>
        </div>
    @endforeach
</div>
<div id="counter" class=""></div>
</section>
