@extends('front.layouts.app')
@section('content')
<section id="testimonial" class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="section-title pt-3">
                    <h1 class="text-uppercase">{{__('testimonial::testimonial.frontpagetitle')}}</h1>
                </div>
            </div>
        </div>
        @if(count($testimonials) > 0)
        <div class="row card-columns-testimonial">
            @foreach ($testimonials as $k=>$testimonial)
            <div class="col-lg-4 col-md-6 mb-3">
                    <div class="card h-100">
                    @if(siteconfig('field_testimonial_title_display') == 'Yes')  
                        <div class="embed-responsive embed-responsive-21by9">
                            @php
                            $alttext = $testimonial->image_alt != '' ? $testimonial->image_alt :
                            $testimonial->client_name;
                            @endphp
                            @if($testimonial->image != '')
                            @if(file_exists(public_path('media/testimonial/'.$testimonial->image)))
                            <img src="{{ getImageUrl($testimonial->image, 'testimonial') }}" alt="{{ $alttext }}"
                                class="card-img-top embed-responsive-item" />
                            @else
                            <img src="{{ asset('assets/front/images/logo.png') }}" alt="{{ $alttext }}"
                                class="card-img-top embed-responsive-item" />

                            @endif
                            @else
                            <img src="{{ asset('assets/front/images/logo.png') }}" alt="{{ $alttext }}"
                                class="card-img-top embed-responsive-item" />

                            @endif
                        </div>
                     @endif 
                        <div class="card-body">
                        <a {{ $testimonial->url ? 'href=' . $testimonial->url  : '' }} class="stretched-link" target="_blank"></a>
                            <div>{!! $testimonial->description !!}</div>
                        </div>
                        <div class="card-footer">
                            <p class="text-muted mb-0">
                                @if(!empty($testimonial->client_name) || !empty($testimonial->client_name))
                                By <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                {{ $testimonial->client_name}} @endif
                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                            </p>
                        </div>

                    </div>
                
            </div>
            @endforeach
        </div>
        @else
        <div class="row text-center">
            <h3>{{ __('testimonial::testimonial.no_result') }}</h3>
        </div>
        @endif

    </div>
</section>
@endsection
