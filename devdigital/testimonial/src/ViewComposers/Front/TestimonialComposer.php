<?php

namespace Testimonial\ViewComposers\Front;

use Cache;
use Testimonial\Testimonial;
use Illuminate\Contracts\View\View;

class TestimonialComposer {

    /*
    * Compose Method to Pass Slider Data in View
    */
    public function compose(View $view) {
        $testimonials =  Cache::rememberForever('testimonial-composer', function() {
            return Testimonial::where('status',1)->orderBy('display_order','ASC')->get();
        });
        
        $view->with('testimonials',$testimonials);
    }
}