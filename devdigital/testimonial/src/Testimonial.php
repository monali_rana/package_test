<?php

namespace Testimonial;

use App\Model;

class Testimonial extends Model
{

    /**
     * Overwrite created_by field value with currently logged in user.
     * Set @var has_created_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */
    protected $has_created_by = true;

    /**
     * Overwrite updated_by field value with currently logged in user.
     * Set @var has_updated_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */

    protected $has_updated_by = true;

    /**
     * Define feilds name which have html tags
     * Set @var notStripTags add DB Table column name which column have html tags.
     *
     * @var array
     */

    public static $notStripTags = ['description'];

    /**
     * Cached variables
     * @var array
     */
    protected $cache = ['testimonial-composer'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_name',
        'url',
        'description',
        'image',
        'image_alt',
        'display_order',
        'status',
        'created_by',
        'updated_by'
    ];


    /**
     * The get result based on search criteria.
     * @param  \Illuminate\Http\Request  $request
     * @return object App\Testimonial
     */
    public function getResult($request)
    {
        // Set default parameter values
        $order_by = !empty($request->get('order_by')) ? $request->get('order_by') : 'display_order';
        $order = !empty($request->get('order')) ? $request->get('order') : 'asc';

        // Fetch testimonial list
        $testimonials = new Testimonial;

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            //$searchStr = addCslashes($searchStr, '\\');
            $escape = "ESCAPE '|'";
            if(substr_count($searchStr,"|")){
               $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            // added escape for searching backslash issue DLC-140
            $testimonials = $testimonials->where(function ($query) use ($searchStr,$escape) {
                $query->whereRaw('client_name LIKE ? '.$escape, '%'.$searchStr.'%')
                      ->orWhereRaw('description LIKE ? '.$escape, '%'.$searchStr.'%');
            });
        }

        // Status
        if ($request->get('status') !== null) {
            $testimonials = $testimonials->where('status', $request->get('status'));
        }

        // Order By & Pagination
        $testimonials = $testimonials->orderBy($order_by, $order)->get();

        return $testimonials;
    }

    /**
     * Update sort order as per the new and old position
     *  @param  $fromPosition integer previous display order
     *  @param  $toPosition integer current display order
     *  @param  $id integer current record id
     */
    public function updateSortOrder($fromPosition, $toPosition, $id)
    {
        $testimonials = new Testimonial;

        $int_actual_position = $fromPosition;
        $int_replace_position = $toPosition;
        $int_temp_position = $int_replace_position;
        while ($int_temp_position != $int_actual_position) {
            if ($int_actual_position > $int_replace_position) {
                $testimonials = $testimonials->where('id', '!=', $id)->where('display_order', '<=', $int_actual_position)->where('display_order', '>=', $int_replace_position)->orderBy('display_order', 'asc')->get();
                foreach ($testimonials as $values) {
                    $rowId = $values->id;
                    $sortTestimonial = Testimonial::find($rowId);
                    $sortTestimonial->fill(['display_order' => ($int_temp_position + 1)]);
                    $sortTestimonial->save();
                    $int_temp_position++;
                }
            } elseif ($int_actual_position < $int_replace_position) {
                $testimonials = $testimonials->where('id', '!=', $id)->where('display_order', '<=', $int_replace_position)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'desc')->get();
                foreach ($testimonials as $values) {
                    $rowId = $values->id;
                    $sortTestimonial = Testimonial::find($rowId);
                    $sortTestimonial->fill(['display_order' => ($int_temp_position - 1)]);
                    $sortTestimonial->save();
                    $int_temp_position--;
                }
            }
            if ($int_temp_position == $int_actual_position):
                break;
            endif;
        }

        $currTestimonial = Testimonial::find($id);
        $currTestimonial->fill(['display_order' => $int_replace_position]);
        $currTestimonial->save();
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param  \Illuminate\Support\Collection|array|int  $ids
     * @return int
     */
    public static function destroy($ids)
    {
        $testimonial = new Testimonial;
        if (is_array($ids)) {
            $currentTestimonials = $testimonial->whereIn('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        } else {
            $currentTestimonials = $testimonial->where('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        }

        parent::destroy($ids);

        if (isset($currentTestimonials) && !empty($currentTestimonials)) {
            foreach ($currentTestimonials as $displayorder=> $id) {
                $int_actual_position = $displayorder;
                $testimonial_result = $testimonial->where('id', '!=', $id)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'asc')->get();
                foreach ($testimonial_result as $values) {
                    $rowId = $values->id;
                    $sort = $testimonial->find($rowId);
                    $position = $sort->display_order;
                    $sort->update(['display_order' => ($position - 1)]);
                }
            }
        }
    }
}
