<?php

/*
* Testimonial Routes...
*/
// Admin Routes
Route::prefix('admin')->middleware(['auth', 'admin','web'])->group(function () {
  
    Route::post('testimonial/bulkaction', 'Testimonial\TestimonialController@bulkaction')->name('testimonial.bulkaction');
    Route::get('testimonial/changestatus', 'Testimonial\TestimonialController@changestatus')->name('testimonial.changestatus');
    Route::post('testimonial/sort', 'Testimonial\TestimonialController@sort')->name('testimonial.sort');
    Route::resource('testimonial', 'Testimonial\TestimonialController')->except(['destroy']);
});

// Web Routes
Route::get('/testimonial', 'Testimonial\TestimonialController@frontindex')->name('testimonial');