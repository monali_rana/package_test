<?php

namespace Testimonial;

use Testimonial\Testimonial;
//use App\Events\BulkAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $testimonials = new Testimonial();
        $testimonials = $testimonials->getResult($request);

        // Render view
        return view('testimonial::admin.modules.testimonial.index')->with('testimonials', $testimonials);
    }
    /**
    * Display a listing of the testimonial for front side
    *
    * @return \Illuminate\Http\Response
    */
    public function frontindex()
    {
        // Render view
        $testimonials = new Testimonial();
        $testimonials = Testimonial::orderBy('display_order', 'asc')->where('status', 1)->get();
        // $testimonials = $testimonials->getResult($request);
        return view('testimonial::front.modules.testimonial.index')->with('testimonials', $testimonials);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonial = new Testimonial();
        $maxNumber = Testimonial::count();
        return view('testimonial::admin.modules.testimonial.addedit')->with(['testimonial'=> $testimonial,'maxnumber'=>$maxNumber]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Testimonial $testimonial)
    {
        $request = $this->stripHtmlTags($request, Testimonial::$notStripTags);
        $this->validation($request);

        $data = $request->all();
        //$data['display_order'] = Testimonial::max('display_order') + 1;

        // Save the Testimonial Member Data
        $testimonial->fill($data);
        $testimonial->save();

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('testimonial.edit', ['id' => $testimonial->id])->with("success", __('testimonial::testimonial.create_success', ['title'=>$request->get('client_name')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('testimonial.index')->with("success", __('testimonial::stestimonial.create_success', ['title'=>$request->get('client_name')]));
        } else {
            return redirect()->route('testimonial.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Testimonial $testimonial)
    {
        $maxNumber = Testimonial::count();
        return view('testimonial::admin.modules.testimonial.addedit')->with(['testimonial'=> $testimonial,'maxnumber'=>$maxNumber]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial  $testimonial)
    {
        $request = $this->stripHtmlTags($request, Testimonial::$notStripTags);
        $this->validation($request);

        $oldPosition = $testimonial->display_order;
        // Prepare Data Array
        $data = $request->all();
        $newPosition = $data['display_order'];
        $data['display_order'] = $oldPosition;

        // Save the Data
        $testimonial->fill($data);
        $testimonial->save();

        //update sortorder
        $testimonials = new Testimonial();
        $testimonials->updateSortOrder($oldPosition, $newPosition, $testimonial->id);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('testimonial.edit', ['id' => $testimonial->id])->with("success", __('testimonial::testimonial.update_success', ['title'=>$request->get('client_name')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('testimonial.index')->with("success", __('testimonial::testimonial.update_success', ['title'=>$request->get('client_name')]));
        } else {
            return redirect()->route('testimonial.index');
        }
    }

    /**
     * Validate the Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validation($request)
    {
        $rule1 = [
            'client_name' => 'required',
            'url' => 'nullable|url',
            'description' => 'required',
            'status' => 'required'
        ];
        $rule2=[];
        if (siteconfig('field_testimonial_title_display') == 'Yes') {
            // $rule2=array('image_alt' => 'required_with:image');
        }
        $rules=$rule1+$rule2;
        // Set Error Messages
        $messages = [
            'client_name.required' => 'The author/client name field is required.',
            'url.url' => 'The url format is invalid.',
            'description.required' => 'The description field is required.'
        ];
        $this->validate($request, $rules, $messages);
    }

    /**
     * Apply bulk action on selected items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkaction(Request $request)
    {
        $testimonial = new Testimonial();

        if ($request->get('bulk-action') == 'delete') {
            Testimonial::destroy($request->get('id'));
            $message =  __('testimonial::testimonial.delete_success');
        } elseif ($request->get('bulk-action') == 'active') {
            Testimonial::whereIn('id', $request->get('id'))->update(['status' => 1]);
            $message = __('testimonial::testimonial.active_success');
        } elseif ($request->get('bulk-action') == 'inactive') {
            Testimonial::whereIn('id', $request->get('id'))->update(['status' => 0]);
            $message = __('testimonial::testimonial.inactive_success');
        }
        $testimonial::flushCache($testimonial);
       // event(new BulkAction($testimonial->getTable(), $request->get('id'), $request->get('bulk-action')));
        return redirect()->back()->with('success', $message);
    }

    /**
     * Apply change status
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changestatus(Request $request)
    {
        $testimonial = Testimonial::findOrFail($request->id);
        $testimonial->status = $testimonial->status == 1?0:1;
        $testimonial->save();
        $message = displayMessages('testimonial::testimonial', $testimonial->status);
        return redirect()->back()->with('success', $message);
    }

    /**
     * Change display order in frontend
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $testimonial = new Testimonial();

        $modifiedDates = array();
        foreach ($request->get('display_order') as $orders) {
            $sort_testimonial = Testimonial::find($orders['0']);
            $sort_testimonial->display_order = $orders['1'];
            $sort_testimonial->save();
            $modifiedDates[$orders['0']]['text_data'] = $sort_testimonial->updated_at;
            $modifiedDates[$orders['0']]['sort_data'] = \Carbon\Carbon::parse($sort_testimonial->updated_at)->format('Y/m/d H:i:s');
        }

        $testimonial::flushCache($testimonial);
        return response()->json(['success' => __('testimonial::testimonial.display_order_success'),'data' => $modifiedDates]);
    }
}
