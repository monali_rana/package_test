<?php
    /*
    |--------------------------------------------------------------------------
    | Default Siteconfigs
    |--------------------------------------------------------------------------
    |
    | This option contains all the settings
    |
    */
    return [
        [
            'title' => 'siteconfig.group_testimonial_settings',
            'module' => 'testimonial',
            'inputs' => [
                [
                    'type' => 'select',
                    'name' => 'field_testimonial_title_display',
                    'label' => 'siteconfig.field_testimonial_title_display',
                    'value' => 'No',
                    'rules'=>'',
                    'client_rules' => '',
                    'options' => [
                        'No' => 'No',
                        'Yes' => 'Yes',
                    ]
                ],
            ],
        ],
    ];
