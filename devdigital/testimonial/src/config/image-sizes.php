<?php
/*
|--------------------------------------------------------------------------
| Default images which will generate while image upload
|--------------------------------------------------------------------------
|
| This option contains all available resized images
|
*/

use Spatie\Image\Manipulations;

return [
    'testimonial' => [
        'fit-500x215' => [
            'width' => 500,
            'height' => 215,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN,
            'is_recommended' => true
        ],
        'fit-270x380' => [
            'width' => 270,
            'height' => 380,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN
        ],
        'fit-45x60' => [
            'width' => 45,
            'height' => 60,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN
        ]
    ],
];
