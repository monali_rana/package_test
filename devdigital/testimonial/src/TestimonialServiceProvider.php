<?php

namespace Testimonial;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class TestimonialServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        include __DIR__.'/routes.php';
        $this->app->bind('Testimonial\TestimonialController');
        $this->mergeConfigFrom(
            __DIR__.'/config/image-sizes.php', 'image-sizes'
        );
        $this->mergeConfigFrom(
            __DIR__.'/config/siteconfig.php', 'siteconfig'
        );
     
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views','testimonial');
        $this->loadTranslationsFrom($this->app->basePath(). '/packages/devdigital/testimonial/src/lang','testimonial');
        view()->composer("testimonial::front.modules.testimonial.index","Testimonial\ViewComposers\Front\TestimonialComposer");
        view()->composer("testimonial::front.modules.section.testimonial","Testimonial\ViewComposers\Front\TestimonialComposer");
        
    }

 
}
