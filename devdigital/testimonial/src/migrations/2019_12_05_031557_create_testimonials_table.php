<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('testimonials')) {
            Schema::create('testimonials', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id', 10);
                $table->string('client_name', 255)->nullable();
                $table->string('url', 500)->nullable();
                $table->mediumText('description')->nullable();
                $table->string('image', 255)->nullable();
                $table->string('image_alt', 255);
                $table->integer('display_order')->unsigned()->default(0);
                $table->tinyInteger('status')->default(1);
                $table->integer('created_by')->unsigned();
                $table->integer('updated_by')->unsigned();
                $table->nullableTimestamps();
                $table->softDeletes();
            });
        }

        if (Schema::hasTable('site_configs')) {
            $menu = array(
                array('name' => 'field_testimonial_title_display',
                    'val' => 'Yes',
                    'type' => 'string',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2019-12-04 10:50:21',
                    'updated_at' => '2019-12-04 10:50:21',
                )
            );

            foreach ($menu as $data) {
                DB::table('site_configs')->updateOrInsert($data);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
