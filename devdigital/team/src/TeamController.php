<?php

namespace Team;

use Team\Team;
//use App\Events\BulkAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $team_members = new Team();
        $team_members = $team_members->getResult($request);
        // Render view
        return view('team::admin.modules.team.index')->with('team_members', $team_members);
    }

    public function frontindex(Request $request)
    {
        $team_members = new Team();
        $team_members = $team_members->getResult($request);
        // Render view
        return view('team::front.modules.team.index')->with('our_team', $team_members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Team $team)
    {
        $maxNumber = Team::count();
        return view('team::admin.modules.team.addedit')->with(['team'=> $team,'maxnumber'=>$maxNumber]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team $team
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Team $team)
    {
        $request = $this->stripHtmlTags($request, Team::$notStripTags);
        $this->validation($request);

        $data = $request->all();
        if (empty($data['display_order'])) {
            $data['display_order'] =Team::count() + 1;
        }
        // Save the Team Member Data
        $team->fill($data);
        $team->save();

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('team.edit', ['id' => $team->id])->with("success", __('team::team.create_success', ['name'=>$request->get('first_name').' '.$request->get('last_name')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('team.index')->with("success", __('team::team.create_success', ['name'=>$request->get('first_name').' '.$request->get('last_name')]));
        } else {
            return redirect()->route('team.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Team $team)
    {
        $maxNumber = Team::count();
        return view('team::admin.modules.team.addedit')->with(['team'=> $team,'maxnumber'=>$maxNumber]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $request = $this->stripHtmlTags($request, Team::$notStripTags);
        $this->validation($request);

        $oldPosition = $team->display_order;

        // Prepare Data Array
        $data = $request->all();
        $newPosition = $data['display_order'];
        $data['display_order'] = $oldPosition;

        // Save the Data
        $team->fill($data);
        $team->save();

        //update sortorder
        $teams = new Team();
        $teams->updateSortOrder($oldPosition, $newPosition, $team->id);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('team.edit', ['id' => $team->id])->with("success", __('team::team.update_success', ['name'=>$request->get('first_name').' '.$request->get('last_name')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('team.index')->with("success", __('team::team.update_success', ['name'=>$request->get('first_name').' '.$request->get('last_name')]));
        } else {
            return redirect()->route('team.index');
        }
    }

    /**
     * Validate the Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validation($request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'position' => 'required',
            'short_bio' => [
                'max:300',
                'nullable'
            ],
            'linkedin' => [
                'regex:/(https?)?:?(\/\/)?(([w]{3}||\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/',
                'nullable'
            ],
            'image' => 'required',
            'image_alt' => 'required',
            'status' => 'required'
        ];
        $this->validate($request, $rules);
    }

    /**
     * Apply bulk action on selected items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkaction(Request $request)
    {
        $team = new Team();

        if ($request->get('bulk-action') == 'delete') {
            Team::destroy($request->get('id'));
            $message =  __('team::team.delete_success');
        } elseif ($request->get('bulk-action') == 'active') {
            Team::whereIn('id', $request->get('id'))->update(['status' => 1]);
            $message = __('team::team.active_success');
        } elseif ($request->get('bulk-action') == 'inactive') {
            Team::whereIn('id', $request->get('id'))->update(['status' => 0]);
            $message = __('team::team.inactive_success');
        }
        $team::flushCache($team);
       // event(new BulkAction($team->getTable(), $request->get('id'), $request->get('bulk-action')));
        return redirect()->back()->with('success', $message);
    }

    /**
     * Change display order in frontend
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $team = new Team();

        foreach ($request->get('display_order') as $orders) {
            $sort_team = Team::find($orders['0']);
            $sort_team->fill(['display_order' => $orders['1']]);
            $sort_team->save();
        }

        $team::flushCache($team);
        return response()->json(['success' => __('team::team.display_order_success')]);
    }
    /**
    * Apply change status
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function changestatus(Request $request)
    {
        $team = Team::findOrFail($request->id);
        $team->status = $team->status == 1?0:1;
        $team->save();
        $message = displayMessages('team::team', $team->status);
        return redirect()->back()->with('success', $message);
    }

    /**
     * remove individual User
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::findOrFail($id);
        Team::destroy($id);

        $message = __('team::team.delete_success_individual', ['name'=>$team->first_name.' '.$team->last_name]);
        \Session::flash('success', $message);
        return redirect()->back();
    }
}