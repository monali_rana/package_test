<section class="team-area py-5 my-5" id="team">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 mx-auto text-center">
					<div class="section-title">
						<h2>{!! nl2br(siteconfig('our_team_title')) !!}</h2>
						<p>{!! nl2br(siteconfig('our_team_sub_title')) !!}</p>
					</div>
				</div>
			</div>
			<div class="row teamSlider">
            @empty(!$our_team)
                @foreach ($our_team as $member)
				<div class="col-md-4">
					<div class="single-team">
                         <div class="embed-responsive embed-responsive-1by1">
						<img class="embed-responsive-item" src="{{ getImageUrl($member->image, 'team','fit-300x410') }}" alt="{{$member->image_alt}}">
						<div class="team-hover">
							<h4>{{ $member->first_name }} {{ $member->last_name }} <span>{{ $member->position }}</span></h4>
						</div>
                        </div>
					</div>
                </div>
                @endforeach
            @endempty
			</div>
		</div>
	  </section>