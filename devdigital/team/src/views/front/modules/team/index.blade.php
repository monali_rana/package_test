@extends('front.layouts.app')
@section('content')
<section id="team" class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="section-title pt-3">
                    <h1 class="text-uppercase">{{__('team::team.frontpagetitle')}}</h1>
                </div>
            </div>
        </div>
        @if(isset($our_team) && count($our_team) > 0)
        <div class="row">
            @foreach ($our_team as $member)
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-0 shadow">
                    @if(isset($member->additional_bio))
                    <a href="#aboutModal{{$member->id}}" data-toggle="modal">
                        <img src="{{ getImageUrl($member->image, 'team','fit-300x410') }}" class="card-img-top" alt="{{$member->image_alt}}">
                    </a>
                    @else
                    <img src="{{ getImageUrl($member->image, 'team','fit-300x410') }}" class="card-img-top" alt="{{$member->image_alt}}">
                    @endif
                    <div class="card-body text-center">
                        <h5 class="card-title mb-0">
                        @if(isset($member->additional_bio))    
                        <a href="#aboutModal{{$member->id}}" data-toggle="modal">
                            {{ $member->first_name }} {{ $member->last_name }}
                        </a>
                        @else
                        {{ $member->first_name }} {{ $member->last_name }}
                        @endif
                        </h5>
                        <div class="card-text text-black-50">{{ $member->position }}</div>
                        @empty(!$member->short_bio)
                                <p>{{ $member->short_bio }}</p>
                        @endempty
                        @empty(!$member->linkedin)
                        <div class="card-text text-black-50">
                            <a href="{{$member->linkedin}}" title="linkedin" class="top-social-icon d-flex align-items-center justify-content-center" target="_blank">
                                <span class="icon-linkedin"><img class="svg m-2" src="{{ asset('assets/front/images/svg/icon_linkedIn.svg') }}" alt="linkedin" /></span>
                            </a>
                        </div>
                        @endempty
                        <div class="modal" tabindex="-1" id="aboutModal{{$member->id}}" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">{{ $member->first_name }} {{ $member->last_name }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row-fluid">
                                            <div class="span10 offset1">
                                                <div id="modalTab">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="about">
                                                            <img src="{{ getImageUrl($member->image, 'team','fit-300x410') }}" alt="{{$member->image_alt}}" name="aboutme" height="140" border="0" class="img-circle"></a>
                                                            <h3 class="media-heading">{{ $member->position }}</h3>
                                                            <p>{{ $member->short_bio }}</p>
                                                            <hr>
                                                            <p class="text-left"><strong>{{__('team::team.frontbiography')}}: </strong><br>
                                                                {!! $member->additional_bio !!}
                                                                <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal hide" tabindex="-1" role="dialog" id="aboutModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">✕</button>
                                    <h3>{{ $member->first_name }} {{ $member->last_name }}</h3>
                                </div>
                                <div class="modal-body" style="text-align:center;">
                                    <div class="row-fluid">
                                        <div class="span10 offset1">
                                            <div id="modalTab">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="about">
                                                        <img src="{{ getImageUrl($member->image, 'team','fit-300x410') }}" alt="{{$member->image_alt}}" name="aboutme" height="140" border="0" class="img-circle"></a>
                                                        <h3 class="media-heading">{{ $member->position }}</h3>
                                                        <hr>
                                                        <p class="text-left"><strong>Bio: </strong><br>
                                                            {{ $member->additional_bio }}
                                                            <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="row text-center">
            <h3>{{__('team::team.no_result')}}</h3> 
        </div>
        @endif
    </div>
</section>
@endsection