@extends('admin.layouts.app')
@section('content')
<div class="row mx-0 mb-3">
<div class="col-6">
    <h1 class="page-title">{{ __('team::team.pagetitle') }}</h1>
</div>
    <div class="top-btn-box col-6" id="normal_btns">
    <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a tabindex="1" href="javascript:void(0)" class="btn search-btn btn-sm show hide btn-primary mr-1" id="search-btn">
                <i class="icon-search-icon top-icon"></i>
                <span class="btn-title">{{ __('team::common.search') }}</span>
            </a>
            <a tabindex="2" href="{{route('team.create')}}" class="btn addnew-btn btn-sm btn-primary mr-1" id="add-btn">
                <i class="icon-addnew top-icon"></i>
                <span class="btn-title">{{ __('team::common.add') }}</span>
            </a>
        </div>
    </div>
    <div class="top-btn-box hide col-6" id="action_btns">
    <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a href="javascript:void(0)" class="btn btn-sm active-btn btn-primary mr-1" onclick="submitactionform('active');">
                <i class="icon-radiobutton_checked top-icon"></i>
                <span class="btn-title">{{ __('team::common.active') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm inactive-btn btn-primary mr-1" onclick="submitactionform('inactive');">
                <i class="icon-radio_button_unchecked top-icon"></i>
                <span class="btn-title">{{ __('team::common.inactive') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm delete-btn btn-dark" onclick="submitactionform('delete');">
                <i class="icon-close-icon top-icon"></i>
                <span class="btn-title">{{ __('team::common.delete') }}</span>
            </a>
        </div>
    </div>
</div>
    <div class="col-12 admin-holder">
        <div class="row">
            <div class="{{ Request::has('status') && Request::has('search') ? 'show' : 'hide' }}" id="searchbox">
                <form name="frmsearch" id="frmsearch" action="{{ route('team.index') }}" method="GET" class="col-12">
                    @foreach (Request::all() as $key=>$value)
                        @if (in_array($key,['search','status','btnsearch']))
                            @continue
                        @else
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                        @endif
                    @endforeach
                    <div class="row">
                        <div class="form-group col-md-6 col-12">
                            <label>{{ __('team::team.name') }}</label>
                            <input tabindex="3" name="search" id="search" placeholder="{{ __('team::common.search') ." ". __('team::team.pagetitle') }}" type="text" class="form-control" value="{{ Request::get('search') }}">
                        </div>
                        <div class="form-group col-xl-2 col-lg-4 col-md-5 col-12">
                            <label>{{ __('team::team.status') }}</label>
                            <select tabindex="4" name="status" id="status" class="form-control">
                                <option value="">{{ __('team::common.select_status') }}</option>
                                @foreach (config('status') as $value => $label)
                                    <option value="{{$value}}" {{ Request::get('status') != "" && intval(Request::get('status')) === $value ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <button tabindex="5" type="submit" class="btn submit-btn btn-primary" id="btnsearch" name="btnsearch">{{ __('team::common.search') }}</button>
                            <a href="{{ route('team.index', ['search' => '', 'status' => '']) }}"  class="btn btn-primary reset-btn">{{ __('team::common.reset') }}</a>
                            <button tabindex="7" type="button" class="btn close-btn btn-dark" id="search-btn-h">{{ __('team::common.close') }}</button>
                            <hr>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 sortable-main">
                <section id="wrapper">
                    <form name="frmlist" id="frmlist" action="{{ route('team.bulkaction') }}" method="POST">
                        @csrf
                        <input type="hidden" name="bulk-action" value="">
                        <table  data-orders="6" data-target="3"  data-sort-url="{{ route('team.sort') }}" class="sort_table table table-hover mb-0" width="100%">
                            <thead>
                                <tr>
                                    <th class="hide" scope="col">

                                    </th>
                                    <th class="active-box status-column" scope="col">
                                    <i  class="sort"></i>
                                    </th>
                                    <th class="check-box nosort" scope="col">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input" name="selectAll" id="selectAll" onclick="checkAll();">
                                        <label class="custom-control-label" for="selectAll">&nbsp;</label>
                                    </div>
                                    </th>
                                    <th scope="col" class="control nosort"> <!-- this blank column is responsive controll -->

                                    </th>
                                    <th scope="col">
                                        <span>{{ __('team::team.name') }}</span>
                                    </th>
                                    <th scope="col">
                                        <span>{{ __('team::team.position') }}</span>
                                    </th>
                                    <th scope="col">
                                            <span>{{ __('team::team.display_order') }}</span>
                                    </th>
                                    <th class="text-right nosort" scope="col">{{__('team::common.edit')}}</th>
                                </tr>
                            </thead>
                            <tbody class="list-section" >
                            @foreach ($team_members as $team)
                                <tr id="display_order_{{ $team->id }}">
                                   <td class="hide">
                                                {{$team->id}}
                                    </td>
                                    <td class="active-box">
                                    <a href="{{ route('team.changestatus', ['id' => $team->id]) }} ">
                                           <i style="display:none">{{$team->status}}</i>
                                        <span class="sort {{ $team->status == 1 ? 'active' : 'inactive' }} "></span>
                                    </a>
                                    </td>
                                    <td class="check-box">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input chkbox action-checkbox" id="filled-in-box_{{ $team->id }}" name="id[]" value="{{$team->id}}">
                                        <label class="custom-control-label" for="filled-in-box_{{ $team->id }}">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td class="media">
                                        <div class="thumb">
                                            @if (!empty($team->image))
                                                <img src="{{ getImageUrl($team->image, 'team', 'fit-45x45') }}" class="rounded mr-2"/>
                                            @endif
                                        </div>
                                        <div class="title">
                                            {{ $team->first_name }} {{ $team->last_name }}
                                        </div>
                                    </td>
                                    <td>
                                        {{ $team->position }}
                                    </td>
                                    <td class="row_sorting">
                                        {{ $team->display_order }}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('team.edit', ['id' => $team->id]) }}"><i class="icon-edit-icon"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if (count($team_members) == 0)
                                <tr class="noreocrd">
                                    <td colspan="7" class="text-center">
                                        {{ __('team::team.no_result') }}
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </form>
                </section>
            </div>
        </div>
    </div>
@endsection
