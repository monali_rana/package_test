@extends('admin.layouts.app')
@section('content')

<form method="post" action="{{ (isset($team->id) ? route('team.update',$team->id) : route('team.store'))  }}"
    name="frmaddedit" id="frmaddedit">
    <div class="row mx-0 mb-3">
        <div class="col-lg-6">
            <h1 class="page-title">{{ (isset($team->id) ? __('team::team.editpagetitle') : __('team::team.addpagetitle'))  }}</h1>
        </div>
        <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center align-items-center">
            <div class="top-btn-box">
                <div class="top-btn-box d-flex">
                    <a tabindex="5" href="{{ route('team.index') }}" class="btn btn-sm btn-dark mr-1"><i
                            class="icon-close-icon top-icon"></i> <span>{{ __('team::common.cancel') }}</span></a>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='save'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('team::common.save') }}</span></button>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary" id="btnsave" name="btnsave"
                        value='savecontinue'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('team::common.savecontinue') }}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @csrf
        @if(isset($team->id)) @method('PUT') @endif
        <div class="row">
            <div class="form-group col-md-6">
                <label for="first_name">{{ __('team::team.first_name') }}<span class="text-danger">*</span></label>
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror"
                    name="first_name" value="{{old('first_name',$team->first_name)}}" data-validator="required">
                <div class="errormessage">@error('first_name') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                <label for="last_name">{{ __('team::team.last_name') }}<span class="text-danger">*</span></label>
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"
                    name="last_name" value="{{old('last_name',$team->last_name)}}" data-validator="required">
                <div class="errormessage">@error('last_name') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                <label for="position">{{ __('team::team.position') }}<span class="text-danger">*</span></label>
                <input id="position" type="text" class="form-control @error('position') is-invalid @enderror"
                    name="position" value="{{old('position',$team->position)}}" data-validator="required">
                <div class="errormessage">@error('position') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                <label for="linkedin">{{ __('team::team.linkedin') }}</label>
                <input id="linkedin" type="text" class="form-control @error('linkedin') is-invalid @enderror"
                    name="linkedin" value="{{old('linkedin',$team->linkedin)}}" data-validator="url">
                <small class="text-mute">{{ __('team::team.linkedin') }} must contain <b>http://linkedin.com/</b> or
                    <b>https://linkedin.com/</b></small>
                <div class="errormessage">@error('linkedin') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                @component('admin.component.status_dropdown',['refModel'=>'team','status'=>
                $team->status,'required'=>false,'default'=>1] )
                @endcomponent
            </div>
            <div class="form-group col-md-6">
                <label for="display_order">{{ __('team::team.display_order') }}</label>

                @if(isset($team->id))
                <select class="form-control" id="display_order" name="display_order">
                    @for ($i = 1; $i <= $maxnumber; $i++) <option value="{{ $i }}" @if( old('display_order', $team->
                        display_order) == $i ) selected @endif>{{ $i }}</option>
                        @endfor
                </select>
                @else
                <input id="display_order" type="text" class="form-control @error('display_order') is-invalid @enderror"
                    name="display_order" value="{{$maxnumber + 1 }}" readonly>
                @endif
                @error('display_order')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-md-12">
                <label for="short_bio">{{ __('team::team.short_bio') }}</label>
                <textarea id="short_bio" class="form-control @error('short_bio') is-invalid @enderror" name="short_bio"
                    rows="3" data-validator="max:300">{{old('short_bio',$team->short_bio)}}</textarea>
                <div class="errormessage">@error('short_bio') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-12">
                <label for="additional_bio">{{ __('team::team.additional_bio') }}</label>
                <textarea id="additional_bio" class="tinymce form-control @error('additional_bio') is-invalid @enderror"
                    name="additional_bio" rows="20">{{old('additional_bio',$team->additional_bio)}}</textarea>
                @error('additional_bio')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="temp_image">{{ __('team::team.image') }}<span class="text-danger">*</span></label>
                <input type="file" data-size="{{ getImageUploadSizeInMB('team') }}" class="dropify image-upload"
                    name="temp_image" data-folder="team"
                    {{ old('image',$team->image) != "" ? 'data-default-file='.getImageUrl(old('image',$team->image),'team') : ''}}
                    @if(old('image',$team->image)) @else data-validator="required" @endif />
                <div><small>{{ getImageRecommendedSize('team') }}</small></div>
                <input name="image" type="hidden" value="{{old('image',$team->image)}}" data-max-file-size="5M">
                <div id="error_image" class="errormessage  @error('image') @else{{  'd-none'  }} @enderror">
                    @error('image'){{ $message }}@enderror</div>
            </div>


            <div class="form-group col-md-6">
                <label for="image_alt">{{ __('team::team.image_alt') }}<span class="text-danger">*</span></label>
                <input id="image_alt" type="text" class="form-control @error('image_alt') is-invalid @enderror"
                    name="image_alt" value="{{old('image_alt',$team->image_alt)}}" data-validator="required">
                <div class="errormessage">@error('image_alt') {{ $message }} @enderror</div>
            </div>

            <div class="col-md-12">
                <h2>{{ __('team::team.seo') }}</h2>
            </div>
            <div class="form-group col-md-6">
                <label for="meta_title">{{ __('team::team.meta_title') }}</label>
                <input id="meta_title" type="text" class="form-control @error('meta_title') is-invalid @enderror"
                    name="meta_title" value="{{old('meta_title',$team->meta_title)}}">
                @error('meta_title')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="meta_desc">{{ __('team::team.meta_desc') }}</label>
                <textarea id="meta_desc" class="form-control @error('meta_desc') is-invalid @enderror" name="meta_desc"
                    rows="5">{{old('meta_desc',$team->meta_desc)}}</textarea>
                @error('meta_desc')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
</form>
@endsection
