<?php

namespace Team;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class TeamServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        include __DIR__.'/routes.php';
        $this->app->bind('Team\TeamController');
        $this->mergeConfigFrom(
            __DIR__.'/config/image-sizes.php', 'image-sizes'
        );
       $this->mergeConfigFrom(
            __DIR__.'/config/siteconfig.php', 'siteconfig'
        ); 
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views','team');
        $this->loadTranslationsFrom($this->app->basePath(). '/packages/devdigital/team/src/lang','team');
        View::composer("team::front.modules.team.index","Team\ViewComposers\Front\TeamComposer");
        View::composer("team::front.modules.section.ourteam","Team\ViewComposers\Front\TeamComposer");
    }

 
}
