<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('teams')) {
            Schema::create('teams', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id',10);
                $table->string('first_name', 255)->nullable();
                $table->string('last_name', 255)->nullable();
                $table->string('image', 255)->nullable();
                $table->string('image_alt', 255);
                $table->string('position', 255)->nullable();
                $table->string('short_bio', 300)->nullable();
                $table->string('linkedin', 255)->nullable();
                $table->text('additional_bio')->nullable();
                $table->string('meta_title')->nullable();
                $table->text('meta_desc')->nullable();
                $table->integer('display_order')->unsigned();
                $table->tinyInteger('status')->default(1);
                $table->integer('created_by')->unsigned();
                $table->integer('updated_by')->unsigned();
                $table->nullableTimestamps();
                $table->softDeletes();
            });
        }
        if (Schema::hasTable('site_configs')) {
            $menu = array(
                array('name' => 'our_team_title',
                    'val' => 'Why The laravelmvc Team?',
                    'type' => 'string',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2019-12-04 10:50:21',
                    'updated_at' => '2019-12-04 10:50:21',
                ),
                array('name' => 'our_team_sub_title',
                    'val' => 'Who are  we',
                    'type' => 'string',
                    'created_by' => '1',
                    'updated_by' => '1',
                    'created_at' => '2019-12-05 10:50:21',
                    'updated_at' => '2019-12-05 10:50:21',
                )
            );

            foreach ($menu as $data) {
                DB::table('site_configs')->updateOrInsert($data);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
