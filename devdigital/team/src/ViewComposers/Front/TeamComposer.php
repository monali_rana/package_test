<?php

namespace Team\ViewComposers\Front;


use Cache;
use Team\Team;
use Illuminate\Contracts\View\View;

class TeamComposer {

    /*
    * Compose Method to Pass Slider Data in View
    */
    public function compose(View $view) {
        $our_team =  Cache::rememberForever('team-composer', function() {
            return Team::where('status',1)->orderBy('display_order','ASC')->get();
        });
        
       
        $view->with('our_team',$our_team);
    }
}