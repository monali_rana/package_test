<?php
    /*
    |--------------------------------------------------------------------------
    | Default Siteconfigs
    |--------------------------------------------------------------------------
    |
    | This option contains all the settings
    |
    */
    return [
        [
            'title' => 'siteconfig.group_our_team_settings',
            'module' => 'team',
            'inputs' => [
                [
                    'type' => 'textarea',
                    'name' => 'our_team_title',
                    'label' => 'siteconfig.field_our_team_title',
                    'rules' => 'required',
                    'client_rules' => 'required',
                    'data-validator-label' => 'title',
                    'value' => 'Why The Laravel CMS Team?',
                    'required' => true
                ],
                [
                    'type' => 'textarea',
                    'name' => 'our_team_sub_title',
                    'label' => 'siteconfig.field_our_team_sub_title',
                    'rules' => 'required',
                    'client_rules' => 'required',
                    'data-validator-label' => 'sub_title',
                    'value' => 'Who are we',
                    'required' => true
                ],
            ],
        ],
    ];
