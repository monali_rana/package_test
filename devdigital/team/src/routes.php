<?php

/*
* Team Routes...
*/
// Admin Routes
Route::prefix('admin')->middleware(['auth', 'admin','web'])->group(function () {
    Route::post('team/bulkaction', 'Team\TeamController@bulkaction')->name('team.bulkaction');
    Route::get('team/changestatus', 'Team\TeamController@changestatus')->name('team.changestatus');
    Route::post('team/sort', 'Team\TeamController@sort')->name('team.sort');
    Route::resource('team', 'Team\TeamController')->except(['destroy']);
});

// Web Routes
Route::get('/team', 'Team\TeamController@frontindex')->name('team');