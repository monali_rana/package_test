<?php

namespace Team;

use App\Model;

class Team extends Model
{
    /**
     * Overwrite created_by field value with currently logged in user.
     * Set @var has_created_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */
    protected $has_created_by = true;

    /**
     * Overwrite updated_by field value with currently logged in user.
     * Set @var has_updated_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */

    protected $has_updated_by = true;

    /**
    * Define feilds name which have html tags
    * Set @var notStripTags add DB Table column name which column have html tags.
    *
    * @var array
    */

    public static $notStripTags = ['additional_bio'];

    /**
     * Cached variables
     * @var array
     */
    protected $cache = ['team-composer'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'position',
        'short_bio',
        'linkedin',
        'additional_bio',
        'meta_title',
        'meta_desc',
        'image',
        'image_alt',
        'display_order',
        'status',
        'created_by',
        'updated_by'
    ];


    /**
     * The get result based on search criteria.
     * @param  \Illuminate\Http\Request  $request
     * @return object App\Team
     */
    public function getResult($request)
    {

        // Set default parameter values
        $order_by = !empty($request->get('order_by')) ? $request->get('order_by') : 'display_order';
        $order = !empty($request->get('order')) ? $request->get('order') : 'asc';

        // Fetch team list
        $team = new Team;

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            //$searchStr = addCslashes($searchStr, '\\');
            $escape = "ESCAPE '|'";
             if(substr_count($searchStr,"|")){
                $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                 $escape = "";
             }
            // added escape for searching backslash issue DLC-140
            $team = $team->where(function ($query) use ($searchStr,$escape) {
                $query
                 ->whereRaw('first_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                 ->orWhereRaw('last_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                 ->orWhereRaw('CONCAT(first_name, " ",last_name) LIKE ?  '.$escape, '%'.$searchStr.'%');
                //->orWhereRaw('CONCAT(first_name, " ",last_name) LIKE ? ', '%'.$searchStr.'%');
            });
        }

        // Status
        if ($request->get('status') !== null) {
            $team = $team->where('status', $request->get('status'));
        }
        // Order By & Pagination
        $team = $team->orderBy($order_by, $order)->get();

        return $team;
    }

    /**
     * Update sort order as per the new and old position
     *  @param  $fromPosition integer previous display order
     *  @param  $toPosition integer current display order
     *  @param  $id integer current record id
     */
    public function updateSortOrder($fromPosition, $toPosition, $id)
    {
        $teams = new Team;

        $int_actual_position = $fromPosition;
        $int_replace_position = $toPosition;
        $int_temp_position = $int_replace_position;
        while ($int_temp_position != $int_actual_position) {
            if ($int_actual_position > $int_replace_position) {
                $teams = $teams->where('id', '!=', $id)->where('display_order', '<=', $int_actual_position)->where('display_order', '>=', $int_replace_position)->orderBy('display_order', 'asc')->get();
                foreach ($teams as $values) {
                    $rowId = $values->id;
                    $sortTeam = Team::find($rowId);
                    $sortTeam->fill(['display_order' => ($int_temp_position + 1)]);
                    $sortTeam->save();
                    $int_temp_position++;
                }
            } elseif ($int_actual_position < $int_replace_position) {
                $teams = $teams->where('id', '!=', $id)->where('display_order', '<=', $int_replace_position)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'desc')->get();
                foreach ($teams as $values) {
                    $rowId = $values->id;
                    $sortTeam = Team::find($rowId);
                    $sortTeam->fill(['display_order' => ($int_temp_position - 1)]);
                    $sortTeam->save();
                    $int_temp_position--;
                }
            }
            if ($int_temp_position == $int_actual_position):
                break;
            endif;
        }

        $currTeam = Team::find($id);
        $currTeam->fill(['display_order' => $int_replace_position]);
        $currTeam->save();
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param  \Illuminate\Support\Collection|array|int  $ids
     * @return int
     */
    public static function destroy($ids)
    {
        $team = new Team;
        if (is_array($ids)) {
            $currentTeams = $team->whereIn('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        } else {
            $currentTeams = $team->where('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        }

        parent::destroy($ids);
        if (isset($currentTeams) && !empty($currentTeams)) {
            foreach ($currentTeams as $displayorder=> $id) {
                //dd($currentFaq);
                $int_actual_position = $displayorder;
                $result = $team->where('id', '!=', $id)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'asc')->get();
                foreach ($result as $values) {
                    $rowId = $values->id;
                    $sortTeam = $team->find($rowId);
                    $position = $sortTeam->display_order;
                    $sortTeam->update(['display_order' => ($position - 1)]);
                }
            }
        }
    }
}
