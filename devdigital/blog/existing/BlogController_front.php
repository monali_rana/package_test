<?php


namespace App\Http\Controllers\Front;

use App\CategoryReference;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
    * Display a listing of the resource.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $request['status'] = 1;
        $categories = new CategoryReference();
        $blogs = new Blog();
        $categories = $categories->getCategoryRefernceDataWithPublishedBlog('blog');
        $blogs = $blogs->getFrontResult($request);

        return view('front.modules.blog.index')->with('blogs', $blogs)->with('categories', $categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($id, $itemId = null)
    {
        $catId = $itemId;
        //if second parameter is greater than zero then catgory blog
        // fisrt id is category id and item_id is blog id
        if (!empty($itemId)) {
            $catId = $id;
            $id = $itemId;
        }

        $blog = new Blog();
        $content = Blog::where('slug', $id)->where('status', 1)->firstOrFail();
        
        if (date('Y-m-d H:i:s', strtotime($content->publish_date)) > date('Y-m-d H:i:s')) {
            return redirect()->route('blog');
        }
        $id =  $content->id;
        $next = $blog->nextBlog($id, $content->publish_date_with_second, $catId);
        $previous = $blog->prevBlog($id, $content->publish_date_with_second, $catId);
        $content->next = $next;
        $content->previous = $previous;

        $categories = new CategoryReference();
        $categories = $categories->getCategoryRefernceDataWithPublishedBlog('blog');

        // dd($content);
        return view('front.modules.blog.show')->with('content', $content)->with('categories', $categories)->with('cat_id', $catId);
    }
}
