@extends('front.modules.blog.layout')
@section('sub_section_title')
{{ $content->title }}
@endsection
@section('sub_section')

{{-- share this script --}}
<div class="row">
    <div class="col-12">
        <ul class="list-inline text-left">
            @if(!empty($content->author_first_name) || !empty($content->author_last_name))<li class="list-inline-item">
                {{ __('blog.author') }} |
                {{$content->author_first_name." ".$content->author_last_name}}</li>@endif
            <li class="list-inline-item">{{ __('blog.date') }} |
                {{ $content->publish_date }}</li>
        </ul>
    </div>
    <!-- Portfolio Item Row -->

    <div class="col-12">
        <div class="embed-responsive embed-responsive-21by9">
            <img class="embed-responsive-item" src="{{ getImageUrl($content->main_image, 'blog') }}"
                alt="{{$content->main_image_alt}}">
        </div>

        <p> {!! $content->description !!}</p>
        <div class="sharethis-inline-share-buttons" st-image="{{ getImageUrl($content->main_image, 'blog') }}"
            st-src="{{ url()->current() }}" st-type="blog" ></div>

    </div>
</div>

<hr>
@if(isset($cat_id) && !empty($cat_id))
@php
$route = 'category.blog.show';
// $param = [$cat_id,];
@endphp
@else
@php
$route = 'blog.show';
@endphp
@endif
<div class="row">
    <div class="col-md-6 col-10 text-truncate pr-5">@if($content->next != null)<a
            href="{{ route( $route, ($cat_id> 0 ? [$cat_id,$content->next->slug]: $content->next->slug) ) }}">{{$content->next->title}}</a>@endif
    </div>
    <div class="col-md-6 col-10 offset-md-0 offset-2 text-right text-truncate pl-5">@if($content->previous
        != null)<a
            href="{{ route( $route, ($cat_id> 0 ? [$cat_id,$content->previous->slug]: $content->previous->slug) ) }}">{{$content->previous->title}}</a>@endif
    </div>
</div>
<hr>
{{-- for comment listing --}}
<div id="ajaxreturnhtml">
    @component('front.component.commentlisting',['title'=>__('comment.comment_listing_title'),'comments'=>$content->commentReference->where('status','=',1)])
    @endcomponent
</div>
{{-- for comment form --}}
@component('front.component.commentform',['action'=>route("comment.add",
[strtolower(class_basename($content)),$content->id]) ])
@endcomponent
<hr>
<div class="row pb-3">
    <div class="col-12">
        <a class="btn btn-dark" href="{{ route('blog')  }}">{{ __('blog.back_link_list') }}</a>
    </div>
</div>


@endsection