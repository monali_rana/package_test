@extends('front.layouts.app')
@section('content')
<section class="container">
    <div class="row">
        <div class="col-12 text-center">
            <div class="section-title pt-3">
                <h1 class="text-uppercase">@yield('sub_section_title')</h1>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            @yield('sub_section')
        </div>
        <div class="col-lg-3">
            <div class="card mb-4">
                <h5 class="card-header">{{__('blog.search')}}</h5>
                <div class="card-body">

                    <form method="GET" action="{{ route('blog') }}">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" value="{{ Request::get('search') }}"
                                placeholder="Search for...">
                            <div class="input-group-append">
                                @if(Request::get('search') == '')<button class="btn btn-outline-secondary" type="submit"><img
                                        src="{{ asset('assets/front/images/search.svg') }}"
                                        alt="{{ __('common.search') }}"
                                        class="mw-100 mt-1 align-top" /></button>@endif
                                @if(Request::get('search') != '')<a href="{{ route('blog') }}"
                                    class="btn btn-outline-secondary reset-btn" type="reset"><img
                                        src="{{ asset('assets/front/images/close.svg') }}"
                                        alt="{{ __('common.reset') }}" 
                                        class="mw-100 mt-1 align-top"/></a>@endif
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card">
            @php $catid = isset($cat_id) && !empty($cat_id) ? $cat_id : 0 @endphp
            @component('front.component.categoryblock',['categories'=>$categories,'cat_id'=>$catid])
            @endcomponent
        </div>
    </div>
</div>
</div>
@endsection