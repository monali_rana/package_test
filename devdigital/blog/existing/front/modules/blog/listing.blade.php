@if(count($blogs) > 0)
<div class="row">
    @foreach ($blogs as $k=>$blog)
    <div class="col-12 mb-3">
        <div class="card">
            <div class="embed-responsive embed-responsive-21by9">
                <a @if(!empty($cat_id) && isset($cat_id))
                    href="{{ route( 'category.blog.show', [$cat_id,$blog->slug] ) }}" @else
                    href="{{ route( 'blog.show', $blog->slug ) }}" @endif>

                    <img src="{{ getImageUrl($blog->main_image, 'blog') }}" alt="{{$blog->main_image_alt}}"
                        class="card-img-top embed-responsive-item">
                </a>
            </div>
            <div class="card-body">
                <p class="text-muted">
                    @if(!empty($blog->author_first_name) || !empty($blog->author_last_name))
                    By
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>

                    {{$blog->author_first_name." ".$blog->author_last_name}} | @endif
                    <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                    {{ $blog->publish_date }}</p>
                <h3>
                    <a @if(!empty($cat_id) && isset($cat_id))
                        href="{{ route( 'category.blog.show', [$cat_id,$blog->slug] ) }}" @else
                        href="{{ route( 'blog.show', $blog->slug ) }}" @endif>
                        {{ $blog->title }}</a></h3>
                <p>{!! readMoreDescription(strip_tags($blog->description)) !!}</p>
                @if(!empty($cat_id) && isset($cat_id))
                <a href="{{ route( 'category.blog.show', [$cat_id,$blog->slug] ) }}"
                    class="btn btn-primary">{{ __('blog.read_more') }}</a>
                @else
                <a href="{{ route( 'blog.show', $blog->slug ) }}" class="btn btn-primary">{{ __('blog.read_more') }}</a>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
<hr>
@include('front.includes.pagination', ['model' => $blogs,'next' => __('blog.next'), 'previous' => __('blog.previous')])
@else
<div class="row text-center">
    <h3>{{ __('blog.no_records') }}</h3>
</div>
@endif