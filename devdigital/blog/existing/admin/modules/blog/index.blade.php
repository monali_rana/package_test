@extends('admin.layouts.app')
@section('content')
<div class="row mx-0 mb-3">
    <div class="col-6">
        <h1 class="page-title"><?php echo __('blog.pagetitle'); ?></h1>
    </div>
    <div class="top-btn-box col-6" id="normal_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a tabindex="1" href="javascript:void(0)" class="btn search-btn btn-sm btn-primary mr-1 show hide"
                id="search-btn">
                <i class="icon-search-icon top-icon"></i>
                <span class="btn-title">{{ __('common.search') }}</span>
            </a>
            <a tabindex="2" href="{{route('blog.create')}}" class="btn addnew-btn btn-primary btn-sm" id="add-btn">
                <i class="icon-addnew top-icon"></i>
                <span class="btn-title">{{ __('common.add') }}</span>
            </a>
        </div>
    </div>
    <div class="top-btn-box hide col-6" id="action_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a href="javascript:void(0)" class="btn btn-sm active-btn btn-primary mr-1"
                onclick="submitactionform('active');">
                <i class="icon-radiobutton_checked top-icon"></i>
                <span class="btn-title">{{ __('common.active') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm inactive-btn btn-primary mr-1"
                onclick="submitactionform('inactive');">
                <i class="icon-radio_button_unchecked top-icon"></i>
                <span class="btn-title">{{ __('common.inactive') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm delete-btn btn-dark" onclick="submitactionform('delete');">
                <i class="icon-close-icon top-icon"></i>
                <span class="btn-title">{{ __('common.delete') }}</span>
            </a>
        </div>
    </div>
</div>
<div class="col-12 admin-holder">
    <div class="row">
        <div class="{{ Request::has('status') && Request::has('search') ? 'show' : 'hide' }}" id="searchbox">
            <form name="frmsearch" id="frmsearch" action="{{ route('blog.index') }}" method="GET" class="col-12">
                @foreach (Request::all() as $key=>$value)
                @if (in_array($key,['search','status','btnsearch','category']))
                @continue
                @else
                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endif
                @endforeach
                <div class="row">
                    <div class="form-group col-md-6 col-12">
                        <label>{{ __('blog.title') }}/{{ __('blog.authorname') }}</label>
                        <input tabindex="3" name="search" id="search"
                            placeholder="{{ __('common.search') ." ". __('blog.pagetitle') }}" type="text"
                            class="form-control" value="{{ Request::get('search') }}">
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-5 col-12">
                        <label>{{ __('blog.status') }}</label>
                        <select tabindex="4" name="status" id="status" class="form-control">
                            <option value="">{{ __('common.select_status') }}</option>
                            @foreach (config('status') as $value => $label)
                            <option value="{{$value}}"
                                {{ Request::get('status') != "" && intval(Request::get('status')) === $value ? 'selected' : '' }}>
                                {{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-5 col-12">
                        <label>{{ __('blog.parent_category') }}</label>
                        <select tabindex="4" name="category" id="category" class="form-control">
                            <option value="">{{ __('blog.select_parent_category') }}</option>
                            @if(!empty($blog_categories))
                                @foreach ($blog_categories as $val)
                                    <option value="{{$val->id}}"  
                                        @if($val->status==0) style="background-color:red;color:white;" @endif    
                                        {{ Request::get('category') != "" && intval(Request::get('category')) === $val->id ? 'selected' : '' }}>
                                        {{ $val->title }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <button tabindex="5" type="submit" class="btn submit-btn btn-primary" id="btnsearch"
                            name="btnsearch">{{ __('common.search') }}</button>
                        <a href="{{ route('blog.index', ['search' => '', 'status' => '', 'category' => '']) }}"
                            class="btn reset-btn btn-primary">{{ __('common.reset') }}</a>
                        <button tabindex="7" type="button" class="btn close-btn btn-dark"
                            id="search-btn-h">{{ __('common.close') }}</button>
                        <hr>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 sortable-main">
            <section id="wrapper">
                <form name="frmlist" id="frmlist" action="{{ route('blog.bulkaction') }}" method="POST">
                    @csrf
                    <input type="hidden" name="bulk-action" value="">
                    <table data-orders="6" data-target="3" class="admintable table table-hover mb-0" width="100%"
                        defaultdir="desc">
                        <thead>
                            <tr>
                                <th class="hide" scope="col">
                                </th>
                                <th class="active-box" scope="col">
                                    <i class="sort"></i>
                                </th>
                                <th class="check-box nosort" scope="col">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input" name="selectAll"
                                            id="selectAll" onclick="checkAll();">
                                        <label class="custom-control-label" for="selectAll">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col" class="nosort control"></th>
                                <th scope="col">
                                    <span>{{ __('blog.title') }}</span>
                                </th>
                                <th scope="col">
                                    <span>{{ __('blog.authorname') }} </span>
                                </th>
                                <th scope="col">
                                    <span>{{  __('blog.publish_date') }}</span>
                                </th>
                                <th scope="col">
                                    <span>{{  __('page.updated-at') }}</span>
                                </th>
                                <th scope="col">
                                    <span>{{ __('blog.parent_category') }}</span>
                                </th>
                                <th scope="col" class="nosort">
                                    <span>{{  __('blog.comment') }}</span>
                                </th>
                                <th class="text-right nosort" scope="col">{{__('common.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody class="list-section">
                            @foreach ($blogs as $blog)
                            <tr id="display_order_{{ $blog->id }}">
                                <td class="active-box hide">
                                    <i style="display:none">{{$blog->created_at}}</i>
                                </td>
                                <td class="active-box">
                                    <a href="{{ route('blog.changestatus', ['id' => $blog->id]) }} ">
                                        <i style="display:none">{{$blog->status}}</i>
                                        <span class="sort {{ $blog->status == 1 ? 'active' : 'inactive' }} "></span>
                                    </a>
                                </td>
                                <td class="check-box">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input chkbox action-checkbox"
                                            id="filled-in-box_{{ $blog->id }}" name="id[]" value="{{$blog->id}}">
                                        <label class="custom-control-label"
                                            for="filled-in-box_{{ $blog->id }}">&nbsp;</label>
                                    </div>
                                </td>
                                <td> </td>
                                <td>
                                    <a href="{{ route('blog.edit', ['id' => $blog->id]) }}">{{ $blog->title }}</a>

                                </td>
                                <td>
                                    {{ $blog->author_first_name }} {{ $blog->author_last_name }}
                                </td>

                                <td data-sort="{{ \Carbon\Carbon::parse( $blog->publish_date)->format('Y/m/d H:i:s')}}">
                                    {{ $blog->publish_date   }} </td>
                                <td data-sort="{{ \Carbon\Carbon::parse( $blog->updated_at)->format('Y/m/d H:i:s')}}">
                                    {{ $blog->updated_at  }}
                                </td>
                                <td>
                                    @if(!empty($blog->cat_title))
                                    {{ $blog->cat_title ?? '' }}
                                    @endif
                                </td>
                                <td>
                                    @if(count($blog->commentReference) == 0)
                                    {{  __('blog.comment').'['.count($blog->commentReference).']' }}
                                    @else
                                    <a
                                        href="{{ route('comment.type.filter', [strtolower(class_basename($blog)),$blog->id]) }}">{{  __('blog.comment').'['.count($blog->commentReference).']' }}</a>
                                    @endif

                                </td>
                                <td class="text-right">
                                    <a href="{{ route('blog.edit', ['id' => $blog->id]) }}"><i
                                            class="icon-edit-icon"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @if (count($blogs) == 0)
                            <tr class="noreocrd">
                                <td colspan="8" class="text-center">
                                    {{ __('blog.no_result') }}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </form>
            </section>
        </div>
    </div>
</div>
@endsection
