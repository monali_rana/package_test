@extends('admin.layouts.app')
@section('content')

<form method="post" action="{{ (isset($blog->id) ? route('blog.update',$blog->id) : route('blog.store'))  }}"
    name="frmaddedit" id="frmaddedit">
    <div class="row mx-0 mb-3">
        <div class="col-lg-6">
            <h1 class="page-title">{{ (isset($blog->id) ? __('blog.editpagetitle') : __('blog.addpagetitle'))  }}</h1>
        </div>
        <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center align-items-center">
            <div class="top-btn-box">
                <div class="top-btn-box d-flex">
                    <a tabindex="5" href="{{ route('blog.index') }}" class="btn btn-sm btn-dark mr-1"><i
                            class="icon-close-icon top-icon"></i> <span>{{ __('common.cancel') }}</span></a>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='save'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('common.save') }}</span></button>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='savecontinue'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('common.savecontinue') }}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">

        @csrf
        @if(isset($blog->id)) @method('PUT') @endif
        <div class="row">
            <div class="form-group col-md-6">
                <label for="title">{{ __('blog.title') }}<span class="text-danger">*</span></label>
                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title"
                    value="{{old('title',$blog->title)}}" data-validator="required">

                <div class="errormessage">@error('title') {{ $message }} @enderror</div>


            </div>
            <div class="form-group col-md-6">
                @component('admin.component.category',['refModel'=>'blog','editedvalue'=> (isset($blog->id) ?
                (isset($blog->categoryReference->category_id)?$blog->categoryReference->category_id:0) :
                0),'required'=>false,'help'=>''] )
                @endcomponent

                {{-- @render('categoryComponent', ['refModel' => 'blog']) --}}
            </div>

            <div class="form-group col-md-6">
                <label for="author_first_name">{{ __('blog.author_first_name') }}</label>
                <input id="author_first_name" type="text"
                    class="form-control @error('author_first_name') is-invalid @enderror" name="author_first_name"
                    value="{{old('author_first_name',$blog->author_first_name)}}">

                <div class="errormessage"> @error('author_first_name') {{ $message }} @enderror</div>

            </div>
            <div class="form-group col-md-6">
                <label for="author_last_name">{{ __('blog.author_last_name') }}</label>
                <input id="author_last_name" type="text"
                    class="form-control @error('author_last_name') is-invalid @enderror" name="author_last_name"
                    value="{{old('author_last_name',$blog->author_last_name)}}">
                <div class="errormessage">@error('author_last_name') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-12">
                <label for="temp_main_image">{{ __('blog.main_image') }}<span class="text-danger">*</span></label>
                <input type="file" data-size="{{ getImageUploadSizeInMB('blog') }}" class="dropify image-upload"
                    name="temp_main_image" data-folder="blog"
                    {{ old('image',$blog->main_image) != "" ? '' : ' data-validator=required ' }}
                    {{ old('image',$blog->main_image) != "" ? 'data-default-file='.getImageUrl(old('image',$blog->main_image),'blog') : ''}} />
                <div><small>{{ getImageRecommendedSize('blog') }}</small></div>
                <input name="main_image" type="hidden" value="{{old('main_image',$blog->main_image)}}">
                <div id="error_main_image" class="errormessage  @error('main_image') @else{{  'd-none'  }} @enderror">
                    @error('main_image'){{ $message }}@enderror</div>
            </div>
            <div class="form-group col-md-6">
                <label for="main_image_alt">{{ __('blog.main_image_alt') }}<span class="text-danger">*</span></label>
                <input id="main_image_alt" type="text"
                    class="form-control @error('main_image_alt') is-invalid @enderror" name="main_image_alt"
                    data-validator="required" value="{{old('main_image_alt',$blog->main_image_alt)}}">

                <div class="errormessage">@error('main_image_alt') {{ $message }} @enderror</div>

            </div>
            <div class="form-group col-md-6">
                <label for="publish_date">{{ __('blog.publish_date') }}<span class="text-danger">*</span></label>
                <input id="publish_date" name="publish_date" type="text"
                    class="form-control datetimepicker @error('publish_date')  @enderror"
                    value="{{old('publish_date',$blog->publish_date)}}" data-validator="required" readonly>
                <div class="errormessage">@error('publish_date') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-6">
                @component('admin.component.status_dropdown',['refModel'=>'blog','status'=>
                $blog->status,'required'=>false,'default'=>1] )
                @endcomponent
            </div>

            <div class="form-group col-md-12">
                <label for="description">{{ __('blog.description') }}<span class="text-danger">*</span></label>
                <textarea id="description" class="tinymce form-control @error('description') is-invalid @enderror"
                    name="description" rows="15"
                    data-validator="required">{{old('description',$blog->description)}}</textarea>

                <div class="errormessage"> @error('description') {{ $message }} @enderror</div>

            </div>
            <div class="col-md-12">
                <h2>{{ __('common.seo') }}</h2>
            </div>
            <div class="form-group col-md-6">
                <label for="meta_title">{{ __('blog.meta_title') }}<span class="text-danger">*</span></label>
                <input id="meta_title" type="text" class="form-control @error('meta_title') is-invalid @enderror"
                    name="meta_title" data-validator="required" value="{{old('meta_title',$blog->meta_title)}}">

                <div class="errormessage">@error('meta_title') {{ $message }} @enderror</div>

            </div>
            <div class="form-group col-md-6">
                <label for="meta_desc">{{ __('blog.meta_desc') }}<span class="text-danger">*</span></label>
                <textarea id="meta_desc" class="form-control @error('meta_desc') is-invalid @enderror" name="meta_desc"
                    rows="5" data-validator-label="meta description"
                    data-validator="required">{{old('meta_desc',$blog->meta_desc)}}</textarea>

                <div class="errormessage"> @error('meta_desc') {{ $message }} @enderror</div>

            </div>
        </div>
    </div>
</form>
@endsection
