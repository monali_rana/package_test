<?php

namespace Blog;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->bind('Blog\BlogController');
        $this->mergeConfigFrom(
            __DIR__.'/config/image-sizes.php',
            'image-sizes'
        );
        $this->mergeConfigFrom(
            __DIR__.'/config/siteconfig.php',
            'siteconfig'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'blog');
        $this->loadTranslationsFrom($this->app->basePath(). '/packages/devdigital/blog/src/lang', 'blog');
        View::composer("blog::front.modules.blog.index", "Blog\ViewComposers\BlogComposer");
    }
}
