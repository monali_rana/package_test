<?php
return [
/* blog Settings */
     [
        'title' => 'siteconfig.group_blog_settings',
        'module' => 'blog',
        'inputs' => [
            [
                'type' => 'text',
                'name' => 'field_blog_per_page_display',
                'label' => 'siteconfig.field_blog_per_page_display',
                'rules' => 'required|integer|gte:1',
                'client_rules' => 'required|integer|min_numeric:1',
                'value' => '10',
                'data-validator-label' => 'blog per page display',
                'required' => true,
            ],
            [
                'type' => 'select',
                'name' => 'comment_moderation',
                'label' => 'siteconfig.comment_moderation',
                'value' => 'No',
                'rules'=>'',
                'client_rules' => '',
                'options' => [
                    'Yes' => 'Yes with mail notification',
                    'Yesw' => 'Yes without mail notification',
                    'No' => 'No',
                ]
            ],
        ],
    ],
];
