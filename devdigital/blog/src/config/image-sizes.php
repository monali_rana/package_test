<?php
/*
|--------------------------------------------------------------------------
| Default images which will generate while image upload
|--------------------------------------------------------------------------
|
| This option contains all available resized images
|
*/

use Spatie\Image\Manipulations;

return [
    'blog' => [
        'fit-setting' => [
            'width' => 800,
            'height' => 342,
            'method' => 'fit',
            'size' => 20,
            'type' => Manipulations::FIT_CONTAIN,
            'is_recommended' => true
        ]
    ]
];
