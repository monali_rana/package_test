<?php

namespace Blog;

use App\CategoryReference;
use Blog\Blog;
use App\Events\BulkAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
    * Display a listing of the resource.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $blogs = new Blog();

        $blogs = $blogs->getResult($request);

        $blogCategories = [];
        if (\Schema::hasTable('category_references')) {
            $blogCategories = new \App\CategoryReference();

            $blogCategories = $blogCategories->getCategoryRefernceDataWithPublishedBlog('blog', 1);
        }

        // Render view
        return view('blog::admin.modules.blog.index')->with(['blogs'=> $blogs,'blog_categories' => $blogCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogs = new Blog();

        return view('blog::admin.modules.blog.addedit')->with('blog', $blogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Blog $blog)
    {
        $request = $this->stripHtmlTags($request, Blog::$notStripTags);
        $this->validation($request);

        $data = $request->all();
        $data['display_order'] = Blog::max('display_order') + 1;
        $data['main_image'] = $data['main_image'];
        $data['main_image_alt'] = $data['main_image_alt'];
        // Save the blog Data
        if (isset($data['publish_date'])) {
            $data['publish_date'] = \Carbon\Carbon::parse($data['publish_date'])->format('Y-m-d H:i').':'.date("s");
        }
        $blog->fill($data);
        $blog->save($data);
        $blog = Blog::find($blog->id);
        $slug = $blog->title;
        $blog->slug = getSlugText($slug, $blog);
        $blog->save();

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('blog.edit', ['id' => $blog->id])->with("success", __('blog.create_success', ['title'=>$request->get('title')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('blog.index')->with("success", __('blog.create_success', ['title'=>$request->get('title')]));
        } else {
            return redirect()->route('blog.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('blog::admin.modules.blog.addedit')->with(['blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $request = $this->stripHtmlTags($request, Blog::$notStripTags);
        $this->validation($request);

        // Prepare Data Array
        $data = $request->all();
        if (isset($data['publish_date'])) {
            if (\Carbon\Carbon::parse($data['publish_date'])->format('Y-m-d H:i') == \Carbon\Carbon::parse($blog->publish_date)->format('Y-m-d H:i')) {
                $data['publish_date'] = \Carbon\Carbon::parse($blog->publish_date)->format('Y-m-d H:i:s');
            } else {
                $data['publish_date'] = \Carbon\Carbon::parse($data['publish_date'])->format('Y-m-d H:i').':'.date("s");
            }
        }
        // Save the Data
        $blog->fill($data);
        $blog->save($data);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('blog.edit', ['id' => $blog->id])->with("success", __('blog.update_success', ['title'=>$request->get('title')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('blog.index')->with("success", __('blog.update_success', ['title'=>$request->get('title')]));
        } else {
            return redirect()->route('blog.index');
        }
    }


    /**
     * Validate the Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validation($request)
    {
        $mode = $request->_method;
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'main_image' => 'required',
            'main_image_alt' => 'required',
            'status' => 'required',
            //'parent_category' => 'required',
            'meta_title' => 'required',
            'meta_desc' => 'required',
        ];
        $rules['publish_date'] = 'required|date';
        //message specify
        $messages = [
            'meta_desc.required' => 'The meta description field is required.',
            'publish_date.required' => 'Please select Publish Date.',
            'publish_date.after' => 'Publish Date must be from tomorrow.'
        ];
        $this->validate($request, $rules, $messages);
    }

    /**
     * Apply bulk action on selected items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkaction(Request $request)
    {
        $blog = new Blog();

        if ($request->get('bulk-action') == 'delete') {
            Blog::destroy($request->get('id'));
            $message = __('blog.delete_success');
        } elseif ($request->get('bulk-action') == 'active') {
            Blog::whereIn('id', $request->get('id'))->update(['status' => 1]);
            $message = __('blog.active_success');
        } elseif ($request->get('bulk-action') == 'inactive') {
            Blog::whereIn('id', $request->get('id'))->update(['status' => 0]);
            $message = __('blog.inactive_success');
        }
        $blog::flushCache($blog);
        event(new BulkAction($blog->getTable(), $request->get('id'), $request->get('bulk-action')));
        return redirect()->back()->with('success', $message);
    }

    /**
     * Update display order of the items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $blog = new Blog();

        foreach ($request->get('display_order') as $display_order => $id) {
            $sort_blog = Blog::find($id);
            $sort_blog->fill(['display_order' => $display_order]);
            $sort_blog->save();
        }
        $blog::flushCache($blog);
        return response()->json(['success' => __('blog.display_order_success')]);
    }

    /**
    * Apply change status
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function changestatus(Request $request)
    {
        $blog = Blog::findOrFail($request->id);
        $blog->status = $blog->status == 1?0:1;
        $blog->save();
        $message = displayMessages('blog', $blog->status);
        return redirect()->back()->with('success', $message);
    }

    /**
    * Display a listing of the resource.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function frontindex(Request $request)
    {
        $request['status'] = 1;
        $categories = new CategoryReference();
        $blogs = new Blog();
        $categories = $categories->getCategoryRefernceDataWithPublishedBlog('blog');
        $blogs = $blogs->getFrontResult($request);

        return view('blog::front.modules.blog.index')->with('blogs', $blogs)->with('categories', $categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($id, $itemId = null)
    {
        $catId = $itemId;
        //if second parameter is greater than zero then catgory blog
        // fisrt id is category id and item_id is blog id
        if (!empty($itemId)) {
            $catId = $id;
            $id = $itemId;
        }

        $blog = new Blog();
        $content = Blog::where('slug', $id)->where('status', 1)->firstOrFail();

        if (date('Y-m-d H:i:s', strtotime($content->publish_date)) > date('Y-m-d H:i:s')) {
            return redirect()->route('blog');
        }
        $id =  $content->id;
        $next = $blog->nextBlog($id, $content->publish_date_with_second, $catId);
        $previous = $blog->prevBlog($id, $content->publish_date_with_second, $catId);
        $content->next = $next;
        $content->previous = $previous;

        $categories = new CategoryReference();
        $categories = $categories->getCategoryRefernceDataWithPublishedBlog('blog');

        // dd($content);
        return view('blog::front.modules.blog.show')->with('content', $content)->with('categories', $categories)->with('cat_id', $catId);
    }
}
