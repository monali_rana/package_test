<?php

namespace Blog;

use App\Model;
use App\CategoryReference;

class Blog extends Model
{

    /**
     * Overwrite created_by field value with currently logged in user.
     * Set @var has_created_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */
    protected $has_created_by = true;

    /**
     * Overwrite updated_by field value with currently logged in user.
     * Set @var has_updated_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */

    protected $has_updated_by = true;

    /**
     * Define feilds name which have html tags
     * Set @var notStripTags add DB Table column name which column have html tags.
     *
     * @var array
     */

    public static $notStripTags = ['description'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'author_first_name',
        'author_last_name',
        'publish_date',
        'description',
        'main_image',
        'main_image_alt',
        'meta_title',
        'meta_desc',
        'display_order',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Cached variables
     * @var array
     */
    protected $cache = ['blog-composer'];

    /**
     * Build Realationship with category reference table to blog
     */
    public function categoryReference()
    {
        return $this->hasOne('App\CategoryReference', 'ref_id', 'id')->where('ref_model_name', '=', 'blog');
    }


    /**
     * set table main column for comment listing page
    */
    public static $itdetityColumn = 'title';

    /**
     * Build Realationship with comment reference table to blog
     */
    public function commentReference()
    {
        return $this->hasMany('App\CommentReference', 'ref_id', 'id')->where('ref_model_name', '=', 'blog')->orderBy('created_at', 'desc');
    }

    /**
    * The get result based on search criteria.
    * @param  \Illuminate\Http\Request  $request
    * @return object App\Blog
    */
    public function getResult($request)
    {
        // Fetch blogs list
        $blogs = new Blog;

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            //$searchStr = addCslashes($searchStr, '\\');
            $escape = "ESCAPE '|'";
            if (substr_count($searchStr, "|")) {
                $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            // added escape for searching backslash issue DLC-140
            $blogs = $blogs->where(
                function ($query) use ($searchStr,$escape) {
                    $query
                    ->whereRaw('blogs.title LIKE ? '.$escape, '%'.$searchStr.'%')
                    ->orWhereRaw('blogs.author_first_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                    ->orWhereRaw('blogs.author_last_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                    //->orWhereRaw('description LIKE ?  '.$escape, '%'.strip_tags($searchStr).'%');
                     ->orWhereRaw('CONCAT(blogs.author_first_name, " ",blogs.author_last_name) LIKE ?  '.$escape, '%'.$searchStr.'%');
                }
            );
        }

        // Status
        if ($request->get('status') !== null) {
            $blogs = $blogs->where('blogs.status', $request->get('status'));
        }

        if (\Schema::hasTable('category_references')) {
            //if category reference is linked then apply join on category and category reference table
            $blogs = $blogs->leftJoin('category_references', function ($join) {
                $join->on('category_references.ref_id', '=', 'blogs.id')
            ->where('category_references.ref_model_name', '=', 'blog')->where('category_references.deleted_at', '=', null);
            });
            // dd($blogs->toSql());
            $blogs = $blogs->leftJoin('categories', 'categories.id', '=', 'category_references.category_id');

            $blogs = $blogs->select('blogs.*', 'category_references.category_id', 'category_references.ref_model_name', 'categories.title as cat_title');

            //category filter
            if (!empty($request->get('category'))) {
                $blogs = $blogs->where('category_id', $request->get('category'));
            }
        }

        // Order By & Pagination
        $blogs = $blogs->get();
        //  dd($blogs);
        return $blogs;
    }

    /**
    * The get front result based on search criteria.
    * @param  \Illuminate\Http\Request  $request
    * @return object App\Blog
    */
    public function getFrontResult($request)
    {
        // Fetch blogs list
        $blogs = new Blog;

        $order_by = !empty($request->get('order_by')) ? $request->get('order_by') : 'publish_date';
        $order = !empty($request->get('order')) ? $request->get('order') : 'desc';

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            //$searchStr = addCslashes($searchStr, "\\");

            $escape = "ESCAPE '|'";
            if (substr_count($searchStr, "|")) {
                $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            $blogs = $blogs->where(
                function ($query) use ($searchStr,$escape) {
                    $query
                    ->whereRaw('title LIKE ? '.$escape, '%'.$searchStr.'%')
                    ->orWhereRaw('author_first_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                    ->orWhereRaw('author_last_name LIKE ?  '.$escape, '%'.$searchStr.'%')
                   // ->orWhereRaw('description LIKE ?  '.$escape, '%'.$searchStr.'%');
                    ->orWhereRaw('CONCAT(author_first_name, " ",author_last_name) LIKE ?  '.$escape, '%'.strip_tags($searchStr).'%');
                }
            );
        }

        if (!empty($request->get('category'))) {
            $blogs = $blogs->whereHas('categoryReference', function ($query) use ($request) {
                $query->where('category_id', $request->get('category'))->where('deleted_at', null);
            });
        }
        if (!empty($request->authornm) && $request->authornm!="") {
            $authorname=str_replace('-', '', $request->authornm);
            $blogs = $blogs->where(
                function ($query) use ($authorname) {
                    $query
                ->whereRaw('CONCAT(author_first_name, "",author_last_name) LIKE ?', '%'.$authorname.'%');
                }
            );
        }
        // Status
        if ($request->get('status') !== null) {
            $blogs = $blogs->where('status', 1);
        }

        $blogs = $blogs->whereRaw('DATE_FORMAT( publish_date ,"%Y-%m-%d %H:%i") <= ?', \Carbon\Carbon::now()->rawFormat("Y-m-d H:i"));
        // Order By & Pagination
        $per_page = siteconfig('field_blog_per_page_display');
        // Order By & Pagination
        $blogs = $blogs->orderBy($order_by, $order)->orderBy('title', 'asc')->paginate($per_page);

        return $blogs;
    }

    /** Override Model Save method
     *
     * @param  array  $options
     * @return bool
    */
    public function save(array $options = [])
    {
        parent::save();

        //save category reference data if category id exist
        $blog = $this;
        $requestData = $options;

        if (isset($requestData['parent_category'])) {
            $categoryRefereceObj = new CategoryReference;

            $data['ref_model_name'] = 'blog';
            $data['ref_id'] = $blog->id;
            $data['category_id'] = $requestData['parent_category'];
            $existing_ref = $categoryRefereceObj->where('ref_model_name', 'blog')->where('ref_id', $blog->id)->get();
            if ($existing_ref->count() > 0) {
                $categoryRefereceObj->where('ref_id', $blog->id)->where('ref_model_name', 'blog')->update(['category_id' => $data['category_id']]);
            } else {
                $categoryRefereceObj->fill($data);
                $categoryRefereceObj->save();
            }
            // Save the Data
        }
        //save category reference data if category id exist
    }

    public function getPublishDateAttribute()
    {
        if (isset($this->attributes['publish_date'])) {
            $timestamp = $this->attributes['publish_date'];
            $date = \Carbon\Carbon::parse($timestamp)->format(config('app.datetime_format'));
            return $date;
        }
    }
    public function getPublishDateWithSecondAttribute()
    {
        if (isset($this->attributes['publish_date'])) {
            $timestamp = $this->attributes['publish_date'];
            // $date = \Carbon\Carbon::parse($timestamp)->format(config('app.datetime_format'));
            return $timestamp;
        }
    }

    /**
     * Override Model Delete method
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        parent::delete();
        //remove category reference data
        $this->CategoryReference()->delete();
    }

    public function nextBlog($id, $publish_date, $cat_id)
    {
        $publish_date = \Carbon\Carbon::parse($publish_date)->format('Y-m-d H:i:s');
        $next = $this->where('publish_date', '>=', $publish_date)->where('id', '!=', $id)->whereRaw('publish_date <= NOW()')->where('status', 1)->orderBy('publish_date', 'asc')->orderBy('title', 'desc')->select('id', 'slug', 'title', 'publish_date');

        if ($cat_id > 0) {
            $next = $next->whereRaw('id in (SELECT ref_id FROM category_references WHERE ref_model_name ="blog" AND category_id = ?) ', $cat_id);
        }

        $next = $next->first();
        return $next;
    }

    public function prevBlog($id, $publish_date, $cat_id)
    {
        $publish_date = \Carbon\Carbon::parse($publish_date)->format('Y-m-d H:i:s');
        $prev = $this->where('publish_date', '<=', $publish_date)->where('id', '!=', $id)->whereRaw('publish_date <= NOW()')->where('status', 1)->orderBy('publish_date', 'desc')->orderBy('title', 'asc')->select('id', 'slug', 'title', 'publish_date');
        if ($cat_id > 0) {
            $prev = $prev->whereRaw('id in (SELECT ref_id FROM category_references WHERE ref_model_name ="blog" AND category_id = ?) ', $cat_id);
        }
        $prev = $prev->first();

        return $prev;
    }
}
