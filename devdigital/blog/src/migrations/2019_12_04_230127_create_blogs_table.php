<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('blogs')) {
            Schema::create('blogs', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integerIncrements('id', 10);
                $table->string('title', 100)->nullable();
                $table->string('slug')->nullable();
                $table->string('author_first_name', 100)->nullable();
                $table->string('author_last_name', 100)->nullable();
                $table->string('publish_date')->nullableTimestamps();
                $table->mediumText('description');
                $table->string('main_image', 100);
                $table->string('main_image_alt', 100);
                $table->integer('blog_category_id')->unsigned()->nullable();
                $table->string('meta_title', 50)->nullable();
                $table->mediumText('meta_desc')->nullable();
                $table->integer('display_order')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->integer('created_by')->unsigned();
                $table->integer('updated_by')->unsigned();
                $table->nullableTimestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
