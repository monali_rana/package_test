<?php

namespace Blog;

use Cache;
use Illuminate\Contracts\View\View;

class BlogComposer
{

    /*
    * Compose Method to Pass Slider Data in View
    */
    public function compose(View $view)
    {
        $other_blogs =  Cache::rememberForever('blog-composer', function () {
            return Blog::where('status', 1)->orderBy('display_order', 'ASC')->get();
        });

        $view->with('other_blogs', $other_blogs);
    }
}
