<?php

/*
* Blog Routes...
*/
// Admin Routes
Route::prefix('admin')->middleware(['auth', 'admin','web'])->group(function () {
    Route::post('blog/bulkaction', 'Blog\BlogController@bulkaction')->name('blog.bulkaction');
    Route::get('blog/changestatus', 'Blog\BlogController@changestatus')->name('blog.changestatus');
    Route::post('blog/sort', 'Blog\BlogController@sort')->name('blog.sort');
    Route::resource('blog', 'Blog\BlogController')->except(['destroy']);
});

// Web Routes
Route::get('/blogs', 'Blog\BlogController@frontindex')->name('blog');
Route::get('/blog/{slug}', 'Blog\BlogController@show')->name('blog.show');
