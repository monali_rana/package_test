@extends('front.modules.blog.layout')
@section('sub_section_title')
{{ __('blog.fronttitle') }}
@endsection
@section('sub_section')

@includeIf('front.modules.blog.listing')

@endsection
