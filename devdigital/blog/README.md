1. Add following thing in Composer.json file- 

  "require": {
        "devdigital/blog": "0.0.1"
    },
    "repositories": [{
        "type": "path",
        "url": "packages/devdigital/blog",
        "options": {
            "symlink": true
        }
    }]



2. After adding package Run following commands - 
* composer dump-autoload
* composer update
* php artisan migrate

3. View files also include with package so developer can change layout with use of these files
