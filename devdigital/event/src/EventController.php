<?php

namespace Event;

use Event\Event;
//use App\Events\BulkAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = new Event();
        $events = $events->getResult($request);

        // Render view
        return view('event::admin.modules.event.index')->with('events', $events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event;
        return view('event::admin.modules.event.addedit')->with('event', $event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Event $event)
    {
        $request = $this->stripHtmlTags($request, Event::$notStripTags);
        $this->validation($request);

        $data = $request->all();

        // Save the event Data
        if (isset($data['start_date'])) {
            $data['start_date'] = \Carbon\Carbon::parse($data['start_date'])->format('Y-m-d H:i:s');
        }
        if (isset($data['end_date'])) {
            $data['end_date'] = \Carbon\Carbon::parse($data['end_date'])->format('Y-m-d H:i:s');
        }

        if (isset($data['event_time'])) {
            $data['event_time'] = \Carbon\Carbon::parse($data['event_time'])->format('H:i:s');
        }
        $event->fill($data);
        $event->save($data);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('event.edit', ['id' => $event->id])->with("success", __('event::event.create_success', ['title'=>$request->get('title')]));
        } elseif ($request->get('btnsave') == 'save') {
           
            return redirect()->route('event.index')->with("success", __('event::event.create_success', ['title'=>$request->get('title')]));
        } else {
            return redirect()->route('event.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('event::admin.modules.event.addedit')->with('event', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request = $this->stripHtmlTags($request, Event::$notStripTags);
        $this->validation($request);
        // Prepare Data Array
        $data = $request->all();
        if (isset($data['start_date'])) {
            $data['start_date'] = \Carbon\Carbon::parse($data['start_date'])->format('Y-m-d H:i:s');
        }
        if (isset($data['end_date'])) {
            $data['end_date'] = \Carbon\Carbon::parse($data['end_date'])->format('Y-m-d H:i:s');
        }
        if (isset($data['event_time'])) {
            $data['event_time'] = \Carbon\Carbon::parse($data['event_time'])->format('H:i:s');
        }
        // Save the Data
        $event->fill($data);
        $event->save($data);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('event.edit', ['id' => $event->id])->with("success", __('event::event.update_success', ['title'=>$request->get('title')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('event.index')->with("success", __('event::event.update_success', ['title'=>$request->get('title')]));
        } else {
            return redirect()->route('event.index');
        }
    }

    /**
     * Validate the Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validation($request)
    {
        // $urlregex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        $rules = [
            'title' => 'required',
            'start_date'      => 'required|date|before_or_equal:end_date',
            'end_date'        => 'required|date|after_or_equal:start_date',
            'event_time' => 'required',
            'url' =>'url'
        ];
        $this->validate($request, $rules);
    }

    /**
    * Apply bulk action on selected items
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function bulkaction(Request $request)
    {
        $event = new Event();

        if ($request->get('bulk-action') == 'delete') {
            Event::destroy($request->get('id'));
            $message = __('event::event.delete_success');
        } elseif ($request->get('bulk-action') == 'active') {
            Event::whereIn('id', $request->get('id'))->update(['status' => 1]);
            $message = __('event::event.active_success');
        } elseif ($request->get('bulk-action') == 'inactive') {
            Event::whereIn('id', $request->get('id'))->update(['status' => 0]);
            $message = __('event::event.inactive_success');
        }
        $event::flushCache($event);
        //event(new BulkAction($event->getTable(), $request->get('id'), $request->get('bulk-action')));
        return redirect()->back()->with('success', $message);
    }

    /**
    * Apply change status
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function changestatus(Request $request)
    {
        $event = Event::findOrFail($request->id);
        $event->status = $event->status == 1?0:1;
        $event->save();
        $message = displayMessages('event::event', $event->status);
        return redirect()->back()->with('success', $message);
    }


    public function frontindex(Request $request)
    {
        
        // Render view
        return view('event::front.modules.event.index');
    }

    /**
     * Filter for events
    */
    public function filter(Request $request)
    {
        $events = new Event();
        $events = $events->getFrontResult($request);

        $resultsEvents = [];
        $i = 0;
        foreach ($events as $event) {
            $i++;
            
            $type = $event->recurrence; // 'Daily','Weekly','Monthly','Yearly'
            $startDate = $event->start_date;
            $endDate = $event->end_date;
            $title = $event->title;
            $description = $event->description;
            $url = $event->url;
            $event_time = $event->event_time;
            $status = $event->status;
            $id = $event->id;

            $body = '';
            if (isset($description) && !empty($description)) {
                $body .= $description.'<br />';
            }
            if (isset($url) && !empty($url)) {
                $body .= 'Link: <a href="'.$url.'" target="_blank">'.$url.'</a>';
            }

            $location = [];
            if (isset($event->address1) && !empty($event->address1)) {
                $location[] = $event->address1;
            }
            if (isset($event->address2) && !empty($event->address2)) {
                $location[] = $event->address2;
            }
            if (isset($event->city) && !empty($event->city)) {
                $location[] = $event->city;
            }
            if (isset($event->state) && !empty($event->state)) {
                $location[] = $event->state;
            }
            if (isset($event->country) && !empty($event->country)) {
                $location[] = $event->country;
            }
            if (isset($event->zipcode) && !empty($event->zipcode)) {
                $location[] = $event->zipcode;
            }
            $location = implode(', ', $location);

            $key = $i;
            $resultsEvents[$key]['title'] = $title;
            $resultsEvents[$key]['description'] = $body;
            $resultsEvents[$key]['url'] = $url;
            $resultsEvents[$key]['start_date'] = str_replace('-', '/', $startDate).' '.$event_time;
            $resultsEvents[$key]['end_date'] = str_replace('-', '/', $endDate).' '.$event_time;
            $resultsEvents[$key]['event_time'] = $event_time;
            $resultsEvents[$key]['recurrence'] = $type;
            $resultsEvents[$key]['status'] = $status;
            $resultsEvents[$key]['location'] = $location;
        }

        return response()->json(['events' => $resultsEvents]);
    }


    // /**
    //  * event detail page
    //   * @param  interger  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     // $event = new Event();
    //     $content = Event::where('id', $id)->where('status', 1)->firstOrFail();

    //     return view('front.modules.event.show')->with('content', $content);
    // }
}
