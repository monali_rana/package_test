<?php

namespace Event;

use App\Model;
use Event\EventReccurences;

class Event extends Model
{
    /**
     * Overwrite created_by field value with currently logged in user.
     * Set @var has_created_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */
    protected $has_created_by = true;

    /**
     * Overwrite updated_by field value with currently logged in user.
     * Set @var has_updated_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */

    protected $has_updated_by = true;

    /**
     * Define feilds name which have html tags
     * Set @var notStripTags add DB Table column name which column have html tags.
     *
     * @var array
     */

    public static $notStripTags = [];

    protected $recurrence = ['Daily','Weekly','Monthly','Yearly'];  //custom

    /**
     * Cached variables
     * @var array
     */
    protected $cache = ['event-composer'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'title',
        'description',
        'url',
        'start_date',
        'end_date',
        'event_time',
        'recurrence',
        'status',
        'address1',
        'address2',
        'city',
        'state',
        'zipcode',
        'country'
    ];

    /**
    * The get result based on search criteria.
    * @param  \Illuminate\Http\Request  $request
    * @return object App\Event
    */
    public function getResult($request)
    {
        // Fetch events list
        $events = new Event;

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            //$searchStr = addCslashes($searchStr, '\\');
            $escape = "ESCAPE '|'";
            if (substr_count($searchStr, "|")) {
                $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            // added escape for searching backslash issue DLC-140
            $events = $events->whereRaw('title LIKE ? '.$escape, '%'.$searchStr.'%');
        }

        // Status
        if ($request->get('status') !== null) {
            $events = $events->where('status', $request->get('status'));
        }

        // Order By & Pagination
        $events = $events->get();

        return $events;
    }

    /**
    * The get result based on search criteria.
    * @param  \Illuminate\Http\Request  $request
    * @return object App\Event
    */
    public function getFrontResult($request)
    {
        // Fetch events list
        $events = new Event;
        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');

            $escape = "ESCAPE '|'";
            if (substr_count($searchStr, "|")) {
                $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            // added escape for searching backslash issue DLC-140
            $events = $events->whereRaw('events.title LIKE ? '.$escape, '%'.$searchStr.'%');
        }

        // Status
        $events = $events->where('events.status', 1);
        $filterDate =  new \Carbon\Carbon(\Carbon\Carbon::parse($request->get('dateFilter'))->format('Y-m-d'));
        $previous = $filterDate->startOfMonth()->subMonth();
        $previousYear = $previous->format('Y');
        $previousMonth = $previous->format('m');
        $next = $filterDate->addMonths(2);
        $nextYear = $next->format('Y');
        $nextMonth = $next->format('m');

        $filterMonth = \Carbon\Carbon::parse($request->get('dateFilter'))->format('m');
        $filterYear = \Carbon\Carbon::parse($request->get('dateFilter'))->format('Y');
        //join with reccrence data
        $events = $events->join((new \Event\EventReccurences)->getTable().' as er', 'er.event_id', 'events.id')
                ->select('events.title', 'events.recurrence', 'events.description', 'events.url', 'events.address1', 'events.address2', 'events.city', 'events.state', 'events.zipcode', 'events.country', 'events.id', 'er.start_date', 'er.end_date', 'er.event_time')
                ->where(function ($query) use ($filterMonth,$filterYear,$previousMonth,$previousYear,$nextMonth,$nextYear) {
                    $query
                    ->where(function ($q) use ($filterMonth,$filterYear) {
                        $q->whereRaw('MONTH(er.start_date) = ?', $filterMonth)->whereRaw('YEAR(er.start_date) = ?', $filterYear);
                    })
                    ->orWhere(function ($q) use ($previousMonth,$previousYear) {
                        $q->whereRaw('MONTH(er.start_date) = ?', $previousMonth)->whereRaw('YEAR(er.start_date) = ?', $previousYear);
                    })
                    ->orWhere(function ($q) use ($nextMonth,$nextYear) {
                        $q->whereRaw('MONTH(er.start_date) = ?', $nextMonth)->whereRaw('YEAR(er.start_date) = ?', $nextYear);
                    });
                })
                ->orderBy('events.title', 'asc');
        
        // Order By & Pagination
        $events = $events->get();

        return $events;
    }

    public function getRecurrence()
    {
        return $this->recurrence;
    }

    public function getRecurrenceOriginal()
    {
        return $this->attributes['recurrence'];
    }

    /** Override Model Save method
     *
     * @param  array  $options
     * @return bool
    */
    public function save(array $options = [])
    {
        $eventreccurences = new EventReccurences;
        parent::save();

        $event = $this;

        //delete current event data
        $eventreccurences->where('event_id', $event->id)->forceDelete();

        //add new data
        $update_events = $this->getRecurrencesEvent($event);

        foreach ($update_events as $k => $v) {
            $eventreccurences->insert($v);
        }
    }

    /**
     *
     * get reccurence event
     */
    public function getRecurrencesEvent($event)
    {
        $event_arr = [];
        $type = $event->getRecurrenceOriginal(); // 'Daily','Weekly','Monthly','Yearly'
        $startDate = $event->start_date;
        $endDate = $event->end_date;
        $title = $event->title;
        $created_by = $event->created_by;
        $updated_by = $event->updated_by;
        $created_at = $event->created_at;
        $updated_at = $event->updated_at;
        $event_time = $event->event_time;
        $status = $event->status;
        $id = $event->id;

        // Start date
        $date = $startDate;
        $i = 0;

        while (strtotime($date) <= strtotime($endDate)) {
            $i++;

            $key = $i;
            $event_arr[$key]['title'] = $title;
            $event_arr[$key]['start_date'] =  \Carbon\Carbon::parse($date)->format('Y/m/d');
            $event_arr[$key]['end_date'] = \Carbon\Carbon::parse($date)->format('Y/m/d');
            $event_arr[$key]['event_time'] = $event_time;
            $event_arr[$key]['status'] = $status;
            $event_arr[$key]['event_id'] = $id;
            $event_arr[$key]['created_by'] = $created_by;
            $event_arr[$key]['updated_by'] = $updated_by;
            $event_arr[$key]['created_at'] = \Carbon\Carbon::parse($created_at)->format('Y/m/d H:i:s');
            $event_arr[$key]['updated_at'] = \Carbon\Carbon::parse($updated_at)->format('Y/m/d H:i:s');
            if ($type == 'Daily') {
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
            if ($type == 'Weekly') {
                $date = date("Y-m-d", strtotime("+7 day", strtotime($date)));
            }
            if ($type == 'Monthly') {
                $date = date("Y-m-d", strtotime("+1 month", strtotime($date)));
            }
            if ($type == 'Yearly') {
                $date = date("Y-m-d", strtotime("+ 1 year", strtotime($date)));
            }
        }
        return $event_arr;
    }
}
