@extends('front.layouts.app')
@section('content')
<script src="{{ asset('assets/vendor/devdigital/libraries/calendar/tui-code-snippet.js')}}"></script>
<script src="{{ asset('assets/vendor/devdigital/libraries/calendar/tui-dom.js')}}"></script>
<script src="{{ asset('assets/vendor/devdigital/libraries/calendar/tui-calendar.js')}}"></script>
<!-- Styles -->
<link href="{{ asset('assets/vendor/devdigital/libraries/calendar/tui-calendar.min.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <div class="section-title pt-3">
                <h1 class="text-uppercase">{{ __('event::event.calendar') }}</h1>
            </div>
        </div>

        <div class="col-md-12 mb-5">
        <div class="loader loader_sub justify-content-center d-none align-items-center h-100 w-100 position-absolute" style="z-index:1">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="d-flex flex-column section-area pt-3 pb-0">
                <!-- Start calender -->
                <input type="hidden" id="calendar_type">
                <input type="hidden" id="calendar_value">
                <div id="menu">
                    <div class="row align-items-center mb-3" id="menu-navi">
                        <div class="col-md-3 d-md-block d-none">
                            <a href="{{ route('calendar') }}"
                                class="btn btn-sm btn-outline-secondary btn-theme ml-auto mr-2">
                                Reset</a></div>
                        <div
                            class="col-6 col-sm-8 col-md-6 d-flex justify-content-center align-items-center text-center">
                            <span class="move-day px-3 py-2" onclick="moveToNextOrPrevRange(-1);"
                                data-action="move-prev">
                                &#10094;
                            </span>
                            <h2 id="renderRange" class="render-range d-inline mx-1 color1 my-0"></h2>
                            <span class="move-day px-3 py-2" onclick="moveToNextOrPrevRange(1);"
                                data-action="move-next">
                                &#10095;
                            </span>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 text-right">
                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-secondary move-today club-calendartype-filter"
                                    onclick="changeToWeekMonth('month',this);" data-action="move-today">Month</button>
                                <button type="button"
                                    class="btn btn-outline-secondary move-today club-calendartype-filter"
                                    onclick="changeToWeekMonth('week',this);" data-action="move-today">Week</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="calendar" style="height: 800px;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        cal.on({
            'clickSchedule': function(event) {
                var schedule = event.schedule;
                // console.log(schedule);
            },
        });

        // Set Calender view (week/month) on hidden element
        $("#calendar_type").val(cal.getViewName());
        // Get current month or week number.
        var fullDate = convertStrintToDate(cal.getDate());
        $("#renderRange").html(convertHumanDate(fullDate));
        $("#calendar_value").val(fullDate);
        // Generate Event on calendar on page load
        generateEventsOnCalendar(fullDate,cal.getViewName());
    });

    // Implement Toust Calender
    var MONTHLY_CUSTOM_THEME = {
        // month header 'dayname'
        'month.dayname.height': '25px',
        'month.dayname.borderLeft': 'none',
        'month.dayname.paddingLeft': '8px',
        'month.dayname.paddingRight': '0',
        'month.dayname.fontSize': '13px',
        'month.dayname.backgroundColor': 'inherit',
        'month.dayname.fontWeight': 'normal',
        'month.dayname.textAlign': 'left',

        // month day grid cell 'day'
        'month.holidayExceptThisMonth.color': '#bbb',
        'month.dayExceptThisMonth.color': '#bbb',
        'month.weekend.backgroundColor': '#fff',
        'month.day.fontSize': '15px',

        // month schedule style
        'month.schedule.borderRadius': '5px',
        'month.schedule.height': '18px',
        'month.schedule.marginTop': '2px',
        'month.schedule.marginLeft': '10px',
        'month.schedule.marginRight': '10px',

        // month more view
        'month.moreView.paddingBottom': '0',
        'month.moreView.border': '1px solid #9a935a',
        'month.moreView.backgroundColor': '#f9f3c6',
        'month.moreViewTitle.height': '28px',
        'month.moreViewTitle.marginBottom': '0',
        'month.moreViewTitle.backgroundColor': '#f4f4f4',
        'month.moreViewTitle.borderBottom': '1px solid #ddd',
        'month.moreViewTitle.padding': '0 10px',
        'month.moreViewList.padding': '10px',
        'common.holiday.color': '#333',

    };

    var cal = new tui.Calendar('#calendar', {
        defaultView: 'month',
        theme: MONTHLY_CUSTOM_THEME, // set theme
        scheduleView: true,
        taskView: false,
        isReadOnly: true,
        useCreationPopup: false,
        useDetailPopup: true
    });

    // Load events on the calendar
    function generateEventsOnCalendar(dateFilter = false, calenderType = false) {

        var data = {
            'dateFilter': dateFilter,
            'calenderType': calenderType,
            'jsonResponse': true,
        };
        $(".loader").addClass('d-flex');
        $.ajax({
            url: "{{ route('event.filter') }}",
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', csrf_token);
            },
            success: function(response) {
                $(".loader").removeClass('d-flex');
                calender_events = [];
                $.each(response.events, function(key, val) {
                    item = {};
                    item['id'] = key;
                    item['calendarId'] = '1';
                    item['title'] = val.title;
                    item['category'] = 'time';
                    item['dueDateClass'] = '';
                    item['start'] = val.start_date;
                    item['end'] = val.end_date;
                    item['isAllDay'] = false;
                    item['body'] = val.description;
                    item['recurrenceRule'] = val.recurrence;
                    item['location'] = val.location;
                    item['time'] = val.event_time;
                    calender_events.push(item);
                });
                cal.clear();
                cal.createSchedules(calender_events,true);
                cal.render();
            },
        });
    }

    // For calender next/previous
    function moveToNextOrPrevRange(val) {
        if (val === -1) {
            cal.prev();
        } else if (val === 1) {
            cal.next();
        }
        var fullDate = convertStrintToDate(cal.getDate());
        $("#renderRange").html(convertHumanDate(fullDate));
        $("#calendar_value").val(fullDate);

        var calenderType = $("#calendar_type").val();
        var dateFilter = $("#calendar_value").val();
        generateEventsOnCalendar(dateFilter, calenderType);
    }

    // Change calender to week when click on the week button
    function changeToWeekMonth(type,ele) {
        cal.changeView(type, true);
        if($(ele).hasClass('btn-outline-secondary')){
            $("button[data-action='move-today']").removeClass('btn-secondary').addClass('btn-outline-secondary');
            $(ele).addClass('btn-secondary').removeClass('btn-outline-secondary');
        }
        $("#calendar_type").val(cal.getViewName());

        var fullDate = convertStrintToDate(cal.getDate());
        $("#calendar_value").val(fullDate);
        $("#renderRange").html(convertHumanDate(fullDate));

        var calenderType = $("#calendar_type").val();
        var dateFilter = $("#calendar_value").val();
        generateEventsOnCalendar(dateFilter, calenderType);
    }

    // Function to Convert string to yyyy-mm-dd format
    function convertStrintToDate(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    function formatdatetimeAsPerStandard(date) {
        var date = new Date(date);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;

            var strTime = month+'/'+day+'/'+year+' '+ hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    function formatdateAsPerStandard(date) {
        var date = new Date(date);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();

        var strTime = month+'/'+day+'/'+year;

        return strTime;
    }

    function convertHumanDate(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2);
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return months[mnth - 1] + " " + date.getFullYear();
    }

</script>
@endsection
