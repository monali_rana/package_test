@extends('admin.layouts.app')
@section('content')


<form method="post" action="{{ (isset($event->id) ? route('event.update',$event->id) : route('event.store'))  }}"
    name="frmaddedit" id="frmaddedit">
    <div class="row mx-0 mb-3">
        <div class="col-lg-6">
            <h1 class="page-title">{{ (isset($event->id) ? __('event::event.editpagetitle') : __('event::event.addpagetitle'))  }}
            </h1>
        </div>
        <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center align-items-center">
            <div class="top-btn-box">
                <div class="top-btn-box d-flex">
                    <a tabindex="5" href="{{ route('event.index') }}" class="btn btn-sm btn-dark mr-1"><i
                            class="icon-close-icon top-icon"></i> <span>{{ __('event::common.cancel') }}</span></a>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='save'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('event::common.save') }}</span></button>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='savecontinue'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('event::common.savecontinue') }}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">

        @csrf
        @if(isset($event->id)) @method('PUT') @endif
        <div class="row">
            <div class="form-group col-md-6">
                <label for="title">{{ __('event::event.title') }}<span class="text-danger">*</span></label>
                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title"
                    value="{{old('title',$event->title)}}" data-validator="required">
                <div class="errormessage">@error('title') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-6">
                @component('event::admin.component.recurrence_dropdown',['refModel'=>'event::event','recurrence_array'=>$event->getRecurrence(),'recurrence'=>
                (isset($event->id) ? $event->recurrence : 'Daily'),'required'=>false,'default'=>'Daily'] )
                @endcomponent
            </div>

            <div class="form-group col-md-6 col-lg-3">
                <label for="start_date">{{ __('event::event.start_date') }}<span class="text-danger">*</span></label>
                <input id="start_date" name="start_date" type="text"
                    class="form-control datepicker @error('start_date') is-invalid @enderror"
                    value="{{old('start_date',displayDate($event->start_date))}}" autocomplete="off"
                    onkeydown="event.preventDefault()" data-validator="required">
                <div class="errormessage">@error('start_date') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-6 col-lg-3">
                <label for="end_date">{{ __('event::event.end_date') }}<span class="text-danger">*</span></label>
                <input id="end_date" name="end_date" type="text"
                    class="form-control datepicker @error('end_date') is-invalid @enderror"
                    value="{{old('end_date',displayDate($event->end_date))}}" autocomplete="off"
                    onkeydown="event.preventDefault()" data-validator="required">
                <div class="errormessage">@error('end_date') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-6 col-lg-3">
                <label for="event_time">{{ __('event::event.event_time') }}<span class="text-danger">*</span></label>
                <input id="event_time" name="event_time" type="text"
                    class="form-control timepicker @error('event_time') is-invalid @enderror"
                    value="{{old('event_time',displayTime($event->event_time))}}" autocomplete="off"
                    onkeydown="event.preventDefault()" data-validator="required">
                <div class="errormessage">@error('event_time') {{ $message }} @enderror</div>
                <input type="hidden" value="{{old('event_time',$event->event_time)}}" id="event_time_hidden" />
            </div>
            <div class="form-group col-md-6 col-lg-3">
                @component('admin.component.status_dropdown',['refModel'=>'event::event','status'=>
                $event->status,'required'=>false,'default'=>1] )
                @endcomponent
            </div>
            <div class="form-group col-md-12">
                <label for="description">{{ __('event::event.description') }}</label>
                <textarea id="description" class="form-control @error('description') is-invalid @enderror"
                    name="description" rows="8">{{old('description',$event->description)}}</textarea>
                @error('description')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="url">{{ __('event::event.url') }}</label>
                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url"
                    value="{{old('url',$event->url)}}" data-validator="url">
                <small class="text-mute">{{ __('event.url') }} must contain <b>http://</b> or <b>https://</b></small>
                <div class="errormessage">@error('url') {{ $message }} @enderror</div>
            </div>

            <div class="form-group col-md-6">
                <label for="address1">{{ __('event::event.address1') }}</label>
                <input id="address1" type="text" class="form-control @error('address1') is-invalid @enderror"
                    name="address1" value="{{old('address1',$event->address1)}}">
                @error('address1')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="address2">{{ __('event::event.address2') }}</label>
                <input id="address2" type="text" class="form-control @error('address2') is-invalid @enderror"
                    name="address2" value="{{old('address2',$event->address2)}}">
                @error('address2')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="city">{{ __('event::event.city') }}</label>
                <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city"
                    value="{{old('city',$event->city)}}">
                @error('city')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="state">{{ __('event::event.state') }}</label>
                <input id="state" type="text" class="form-control @error('state') is-invalid @enderror" name="state"
                    value="{{old('state',$event->state)}}">
                @error('state')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="zipcode">{{ __('event::event.zipcode') }}</label>
                <input id="zipcode" type="text" class="form-control @error('zipcode') is-invalid @enderror"
                    name="zipcode" value="{{old('zipcode',$event->zipcode)}}">
                @error('zipcode')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="country">{{ __('event::event.country') }}</label>
                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror"
                    name="country" value="{{old('country',$event->country)}}">
                @error('country')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
</form>
@endsection
