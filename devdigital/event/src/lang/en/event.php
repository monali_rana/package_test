<?php

return [
    'pagetitle' => 'Events',
    'editpagetitle' => 'Edit Event',
    'addpagetitle' => 'Add Event',
    'no_result' => 'No event(s) found',
    'title' => 'Title',
    'status' => 'Status',
    'create_success' => ':title event added successfully.',
    'update_success' => ':title event updated successfully.',
    'display_order_success' => 'Events display order updated successfully.',
    'active_success' => 'Selected event(s) have been activated successfully.',
    'inactive_success' => 'Selected event(s) have been inactivated successfully.',
    'record_active_success' => 'The event successfully activated.',
    'record_inactive_success' => 'The event successfully inactivated.',
    'delete_success' => 'Selected event(s) have been deleted successfully.',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'event_time' => 'Event Time',
    'description' => 'Description',
    'url' => 'URL',
    'address1' => 'Address 1',
    'address2' => 'Address 2',
    'city' => 'City',
    'state' => 'State',
    'zipcode' => 'Zipcode',
    'country' => 'Country',
    'meta_title' => 'Meta Title',
    'meta_description' => 'Meta Description',
    'recurrence' => 'Recurrence',
    'updated-at' => 'Modified On',


    //front side labels
    'calendar' => 'Calendar',
];
