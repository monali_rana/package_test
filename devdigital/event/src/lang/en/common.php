<?php

return [
    'save' => 'Save',
    'savecontinue' => 'Save & Continue',
    'cancel' => 'Cancel',
    'search' => 'Search',
    'reset' => 'Reset',
    'close' => 'Close',
    'add' => 'Add',
    'edit' => 'Edit',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'delete' => 'Delete',
    'select_all' => 'Select All',
    'view_all' => 'View All',
    'category' => 'Categories',
    'status' => 'Status',
    'select_status' => 'Select Status',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'linkedin' => 'Linkedin',
];
