<?php

/*
* Event Routes...
*/
// Admin Routes
Route::prefix('admin')->middleware(['auth', 'admin','web'])->group(function () {
    Route::post('event/bulkaction', 'Event\EventController@bulkaction')->name('event.bulkaction');
    Route::post('event/sort', 'Event\EventController@sort')->name('event.sort');
    Route::get('event/changestatus', 'Event\EventController@changestatus')->name('event.changestatus');
    Route::resource('event', 'Event\EventController')->except(['destroy']);
});


// Web Routes
Route::get('/events/calendar', 'Event\EventController@frontindex')->name('calendar');
Route::post('/events/filter', 'Event\EventController@filter')->name('event.filter');
// Route::get('/event/{id}', 'Front\EventController@show')->name('event.show');