<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('events')) {
            Schema::create('events', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integerIncrements('id',10)->unsigned();
                $table->date('start_date');
                $table->date('end_date');
                $table->time('event_time');
                $table->enum('recurrence', ['Daily', 'Weekly', 'Monthly', 'Yearly']);
                $table->text('title');
                $table->text('description');
                $table->text('url');
                $table->text('address1');
                $table->text('address2');
                $table->char('city', 255);
                $table->char('state', 100);
                $table->char('zipcode', 20);
                $table->char('country', 100);
                $table->tinyInteger('status')->default(1);
                $table->integer('created_by')->unsigned()->default(0);
                $table->integer('updated_by')->unsigned()->default(0);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
