<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventReccurencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_reccurences')) {
            Schema::create('event_reccurences', function (Blueprint $table) {
                $table->integerIncrements('id',10)->unsigned();
                $table->integer('event_id');
                $table->date('start_date');
                $table->date('end_date');
                $table->time('event_time');
                $table->text('title');
                $table->tinyInteger('status')->default(1)->comment('0- Inactive,1-Active');
                $table->integer('created_by')->unsigned()->nullable()->default(0);
                $table->integer('updated_by')->unsigned()->nullable()->default(0);
                $table->timestamps();
                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_reccurences');
    }
}
