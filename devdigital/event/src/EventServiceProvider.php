<?php

namespace Event;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
         include __DIR__.'/routes.php';
         $this->app->bind('Event\EventController');  
       
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views','event');
        $this->loadTranslationsFrom($this->app->basePath(). '/packages/devdigital/event/src/lang','event');

        $this->publishes([
            __DIR__.'/assets' => public_path('assets/vendor/devdigital'),
        ], 'public');
    }

 
}
