@extends('admin.layouts.app')
@section('content')
<div class="row mx-0 mb-3">
    <div class="col-6">
        <h1 class="page-title"><?php echo __('faq::faq.pagetitle'); ?></h1>
    </div>
    <div class="top-btn-box col-6" id="normal_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a tabindex="1" href="javascript:void(0)" class="btn btn-primary search-btn btn-sm show hide mr-1"
                id="search-btn">
                <i class="icon-search-icon top-icon"></i>
                <span class="btn-title">{{ __('faq::common.search') }}</span>
            </a>
            <a tabindex="2" href="{{route('faq.create')}}" class="btn  btn-primary addnew-btn btn-sm" id="add-btn">
                <i class="icon-addnew top-icon"></i>
                <span class="btn-title">{{ __('faq::common.add') }}</span>
            </a>
        </div>
    </div>
    <div class="top-btn-box col-6 hide" id="action_btns">
        <div class="top-btn-box d-flex justify-content-end align-items-center h-100">
            <a href="javascript:void(0)" class="btn btn-sm active-btn btn-primary mr-1"
                onclick="submitactionform('active');">
                <i class="icon-radiobutton_checked top-icon"></i>
                <span class="btn-title">{{ __('faq::common.active') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm inactive-btn btn-primary mr-1"
                onclick="submitactionform('inactive');">
                <i class="icon-radio_button_unchecked top-icon"></i>
                <span class="btn-title">{{ __('faq::common.inactive') }}</span>
            </a>
            <a href="javascript:void(0)" class="btn btn-sm delete-btn btn-dark" onclick="submitactionform('delete');">
                <i class="icon-close-icon top-icon"></i>
                <span class="btn-title">{{ __('faq::common.delete') }}</span>
            </a>
        </div>
    </div>
</div>
<div class="col-12 admin-holder">
    <div class="row">
        <div class="{{ Request::has('status') && Request::has('search') ? 'show' : 'hide' }}" id="searchbox">
            <form name="frmsearch" id="frmsearch" action="{{ route('faq.index') }}" method="GET" class="col-12">
                @foreach (Request::all() as $key=>$value)
                @if (in_array($key,['search','status','btnsearch']))
                @continue
                @else
                <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endif
                @endforeach
                <div class="row">
                    <div class="form-group col-md-6 col-12">
                        <label>{{ __('faq::faq.question') }}</label>
                        <input tabindex="3" name="search" id="search"
                            placeholder="{{ __('faq::common.search') ." ". __('faq::faq.pagetitle') }}" type="text"
                            class="form-control" value="{{ Request::get('search') }}">
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-5 col-12">
                        <label>{{ __('faq::faq.status') }}</label>
                        <select tabindex="4" name="status" id="status" class="form-control">
                            <option value="">{{ __('faq::common.select_status') }}</option>
                            @foreach (config('status') as $value => $label)
                            <option value="{{$value}}"
                                {{ Request::get('status') != "" && intval(Request::get('status')) === $value ? 'selected' : '' }}>
                                {{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <button tabindex="5" type="submit" class="btn btn-primary submit-btn" id="btnsearch"
                            name="btnsearch">{{ __('faq::common.search') }}</button>
                        <a href="{{ route('faq.index', ['search' => '', 'status' => '']) }}"
                            class="btn btn-primary reset-btn">{{ __('faq::common.reset') }}</a>
                        <button tabindex="7" type="button" class="btn btn-dark close-btn"
                            id="search-btn-h">{{ __('faq::common.close') }}</button>
                        <hr>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 sortable-main">
            <section id="wrapper">
                <form name="frmlist" id="frmlist" action="{{ route('faq.bulkaction') }}" method="POST">
                    @csrf
                    <input type="hidden" name="bulk-action" value="">
                    <table data-orders="5" data-target="3" data-sort-url="{{ route('faq.sort') }}"
                        class="sort_table table table-hover mb-0" width="100%">
                        <thead>
                            <tr>
                                <th class="hide" scope="col"></th>
                                <th class="active-box" scope="col">
                                    <i class="sort"></i>
                                </th>
                                <th class="check-box nosort" scope="col">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input" name="selectAll"
                                            id="selectAll" onclick="checkAll();">
                                        <label class="custom-control-label" for="selectAll">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col" class="control nosort">
                                    <!-- this blank column is responsive controll -->

                                </th>
                                <th scope="col">
                                    <strong>{{ __('faq::faq.question') }}</strong>
                                </th>
                                {{-- <th scope="col">
                                    <strong>{{  __('faq::faq.answer') }}</strong>
                                </th> --}}
                                <th scope="col">
                                    <strong>{{  __('faq::faq.display_order') }}</strong>
                                </th>
                                <th class="text-right nosort" scope="col">{{__('faq::common.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($faqs as $faq)
                            <tr id="display_order_{{ $faq->id }}">
                                <td class="active-box hide">
                                    {{$faq->id}}
                                </td>
                                <td class="active-box">
                                    <a href="{{ route('faq.changestatus', ['id' => $faq->id]) }} ">
                                        <i style="display:none">{{$faq->status}}</i>
                                        <span class="sort {{ $faq->status == 1 ? 'active' : 'inactive' }} "></span>
                                    </a>
                                </td>

                                <td class="check-box">
                                    <div class="custom-control custom-checkbox text-center">
                                        <input type="checkbox" class="custom-control-input chkbox action-checkbox"
                                            id="filled-in-box_{{ $faq->id }}" name="id[]" value="{{$faq->id}}">
                                        <label class="custom-control-label"
                                            for="filled-in-box_{{ $faq->id }}">&nbsp;</label>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    {{ $faq->question }}
                                </td>
                                {{-- <td>
                                        {{ strip_tags( str_limit($faq->answer, $limit = 60, $end = '...')) }}
                                </td> --}}
                                <td class="row_sorting">
                                    {{ $faq->display_order }}
                                </td>
                                <td class="text-right">
                                    <a href="{{ route('faq.edit', ['id' => $faq->id]) }}"><i
                                            class="icon-edit-icon"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @if (count($faqs) == 0)
                            <tr class="noreocrd">
                                <td colspan="7" class="text-center">
                                    {{ __('faq::faq.no_result') }}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </form>
            </section>
        </div>
    </div>
</div>
@endsection
