@extends('admin.layouts.app')
@section('content')


<form method="post" action="{{ (isset($faq->id) ? route('faq.update',$faq->id) : route('faq.store'))  }}"
    name="frmaddedit" id="frmaddedit">
    <div class="row mx-0 mb-3">
        <div class="col-lg-6">
            <h1 class="page-title">{{ (isset($faq->id) ? __('faq::faq.editpagetitle') : __('faq::faq.addpagetitle'))  }}</h1>
        </div>
        <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center align-items-center">
            <div class="top-btn-box">
                <div class="top-btn-box d-flex">
                    <a tabindex="5" href="{{ route('faq.index') }}" class="btn btn-sm btn-dark mr-1"><i
                            class="icon-close-icon top-icon"></i> <span>{{ __('faq::common.cancel') }}</span></a>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary mr-1" id="btnsave" name="btnsave"
                        value='save'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('faq::common.save') }}</span></button>
                    <button tabindex="9" type="submit" class="btn btn-sm btn-primary" id="btnsave" name="btnsave"
                        value='savecontinue'><i class="icon-save_icon top-icon"></i>
                        <span>{{ __('faq::common.savecontinue') }}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @csrf
        @if(isset($faq->id)) @method('PUT') @endif
        <div class="row">
            <div class="form-group col-md-12">
                <label for="question">{{ __('faq::faq.question') }}<span class="text-danger">*</span></label>
                <input id="question" type="text" class="form-control @error('question') is-invalid @enderror"
                    name="question" value="{{old('question',$faq->question)}}" data-validator="required">
                <div class="errormessage">@error('question') {{ $message }} @enderror</div>
            </div>
            <div class="form-group col-md-6">
                @component('admin.component.status_dropdown',['refModel'=>'faq::faq','status'=>
                $faq->status,'required'=>false,'default'=>1] )
                @endcomponent
            </div>
            <div class="form-group col-md-6">
                <label for="display_order">{{ __('faq::faq.display_order') }}</label>

                @if(isset($faq->id))
                <select class="form-control" id="display_order" name="display_order">
                    @for ($i = 1; $i <= $maxnumber; $i++) <option value="{{ $i }}" @if( old('display_order', $faq->
                        display_order) == $i ) selected @endif>{{ $i }}</option>
                        @endfor
                </select>
                @else
                <input id="display_order" type="text" class="form-control @error('display_order') is-invalid @enderror"
                    name="display_order" value="{{$maxnumber + 1 }}" readonly>
                @endif
                @error('display_order')
                <div class="errormessage">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-md-12">
                <label for="answer">{{ __('faq::faq.answer') }}<span class="text-danger">*</span></label>
                <textarea id="answer" class="tinymce form-control @error('answer') is-invalid @enderror" name="answer"
                    rows="15" data-validator="required">{{old('answer', $faq->answer)}}</textarea>
                <div class="errormessage">@error('answer') {{ $message }} @enderror</div>
            </div>
        </div>
    </div>
</form>
@endsection
