<!-- _accordion_ex.scss include for FAQ section-->
<section class="faqs">
    <div class="container">
        <div class="row">
        <div class="col-12 text-center">
                <div class="section-title pt-3">
                    <h1 class="text-uppercase">{{__('faq::faq.frontpagetitle')}}</h1>
                </div>
            </div>
        </div>
        @if(isset($faqs) && count($faqs) > 0)
        <div class="accordion md-accordion accordion-3 mb-5" id="faq-accordions">
            @foreach ($faqs as $k=>$faq)
            <div class="card">
                <div class="card-header p-0" role="tab" id="heading-{{ $k }}">
                    <h3 class="mb-0 ">
                        <a href="javascript:void();" class="text-decoration-none py-3 pl-3 pr-5 d-flex align-items-center text-dark w-100 text-left {{ $k != 0 ? 'collapsed' : '' }} " data-toggle="collapse" data-target="#collapse-{{ $k }}" aria-expanded="{{ $k == 0 ? 'true' : 'false' }}" aria-controls="collapse-{{ $k }}">
                            {{ $faq->question }}
                        </a>
                    </h3>
                </div>
                <div id="collapse-{{ $k }}" class="collapse {{ $k == 0 ? 'show' : '' }}" aria-labelledby="heading-{{ $k }}" data-parent="#faq-accordions">
                    <div class="card-body">
                        {!! $faq->answer !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="row text-center">
            <h3>{{__('faq::faq.no_result')}}</h3>
        </div>
        @endif
    </div>
</section>