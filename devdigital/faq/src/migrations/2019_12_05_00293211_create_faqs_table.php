<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('faqs')) {
            Schema::create('faqs', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id', 10)->unsigned();
                $table->char('question', 100);
                $table->text('answer')->nullable();
                $table->integer('display_order')->unsigned();
                $table->tinyInteger('status')->default(1);
                $table->integer('created_by')->unsigned()->default(0);
                $table->integer('updated_by')->unsigned()->default(0);
                $table->nullableTimestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
