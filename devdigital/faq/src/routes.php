<?php

/*
* FAQ Routes...
*/
// Admin Routes
Route::prefix('admin')->middleware(['auth', 'admin','web'])->group(function () {
    Route::post('faq/bulkaction', 'Faq\FaqController@bulkaction')->name('faq.bulkaction');
    Route::post('faq/sort', 'Faq\FaqController@sort')->name('faq.sort');
    Route::get('faq/changestatus', 'Faq\FaqController@changestatus')->name('faq.changestatus');
    Route::resource('faq', 'Faq\FaqController')->except(['destroy']);
});

// Web Routes
Route::get('/faqs', 'Faq\FaqController@frontindex')->name('faq');