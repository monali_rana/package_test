<?php

namespace Faq;

use App\Model;

class Faq extends Model
{

    /**
     * Overwrite created_by field value with currently logged in user.
     * Set @var has_created_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */
    protected $has_created_by = true;

    /**
     * Overwrite updated_by field value with currently logged in user.
     * Set @var has_updated_by to false if created_by field does not exist in DB Table.
     *
     * @var boolean
     */

    protected $has_updated_by = true;

    /**
     * Define feilds name which have html tags
     * Set @var notStripTags add DB Table column name which column have html tags.
     *
     * @var array
     */

    public static $notStripTags = ['answer'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
        'answer',
        'display_order',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Cached variables
     * @var array
     */
    protected $cache = ['faq-composer'];


    /**
     * The get result based on search criteria.
     * @param  \Illuminate\Http\Request  $request
     * @return object App\Faq
     */
    public function getResult($request)
    {

        // Set default parameter values
        $order_by = !empty($request->get('order_by')) ? $request->get('order_by') : 'display_order';
        $order = !empty($request->get('order')) ? $request->get('order') : 'asc';

        // Fetch faqs list
        $faqs = new Faq;

        // Search
        if (!empty($request->get('search'))) {
            $searchStr = $request->get('search');
            $escape = "ESCAPE '|'";
            if(substr_count($searchStr,"|")){
               $searchStr = str_replace('\\', '\\\\\\', $searchStr);
                $escape = "";
            }
            //$searchStr = addCslashes($searchStr, '\\');
            // added escape for searching backslash issue DLC-140
            $faqs = $faqs->whereRaw('question LIKE ? '.$escape, '%'.$searchStr.'%');
        }

        // Status
        if ($request->get('status') !== null) {
            $faqs = $faqs->where('status', $request->get('status'));
        }

        // Order By & Pagination
        $faqs = $faqs->orderBy($order_by, $order)->get();

        return $faqs;
    }

    /**
     * Update sort order as per the new and old position
     *  @param  $fromPosition integer previous display order
     *  @param  $toPosition integer current display order
     *  @param  $id integer current record id
     */
    public function updateSortOrder($fromPosition, $toPosition, $id)
    {
        $faqs = new Faq;

        $int_actual_position = $fromPosition;
        $int_replace_position = $toPosition;

        $int_temp_position = $int_replace_position;
        while ($int_temp_position != $int_actual_position) {
            if ($int_actual_position > $int_replace_position) {
                $faqs = $faqs->where('id', '!=', $id)->where('display_order', '<=', $int_actual_position)->where('display_order', '>=', $int_replace_position)->orderBy('display_order', 'asc')->get();
                foreach ($faqs as $values) {
                    $rowId = $values->id;
                    $sortFaq = Faq::find($rowId);
                    $sortFaq->fill(['display_order' => ($int_temp_position + 1)]);
                    $sortFaq->save();
                    $int_temp_position++;
                }
            } elseif ($int_actual_position < $int_replace_position) {
                $faqs = $faqs->where('id', '!=', $id)->where('display_order', '<=', $int_replace_position)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'desc')->get();
                foreach ($faqs as $values) {
                    $rowId = $values->id;
                    $sortFaq = Faq::find($rowId);
                    $sortFaq->fill(['display_order' => ($int_temp_position - 1)]);
                    $sortFaq->save();
                    $int_temp_position--;
                }
            }
            if ($int_temp_position == $int_actual_position):
                break;
            endif;
        }

        $currFaq = Faq::find($id);
        $currFaq->fill(['display_order' => $int_replace_position]);
        $currFaq->save();
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param  \Illuminate\Support\Collection|array|int  $ids
     * @return int
     */
    public static function destroy($ids)
    {
        $faq = new Faq;
        if (is_array($ids)) {
            $currentFaqs = $faq->whereIn('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        } else {
            $currentFaqs = $faq->where('id', $ids)->orderBy('display_order', 'desc')->pluck('id', 'display_order');
        }

        parent::destroy($ids);

        if (isset($currentFaqs) && !empty($currentFaqs)) {
            foreach ($currentFaqs as $displayorder=> $id) {
                //dd($currentFaq);
                $int_actual_position = $displayorder;
                $faq_result = $faq->where('id', '!=', $id)->where('display_order', '>=', $int_actual_position)->orderBy('display_order', 'asc')->get();
                foreach ($faq_result as $values) {
                    $rowId = $values->id;
                    $sortFaq = $faq->find($rowId);
                    $position = $sortFaq->display_order;
                    $sortFaq->update(['display_order' => ($position - 1)]);
                }
            }
        }
    }
}
