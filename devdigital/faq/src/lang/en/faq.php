<?php

return [
    'pagetitle' => 'FAQs',
    'frontpagetitle' => 'FAQs',
    'editpagetitle' => 'Edit FAQ',
    'addpagetitle' => 'Add FAQ',
    'no_result' => 'No FAQ(s) found',
    'question' => 'Question',
    'answer' => 'Answer',
    'status' => 'Status',
    'create_success' => ':title added successfully.',
    'update_success' => ':title updated successfully.',
    'display_order_success' => 'FAQs display order updated successfully.',
    'active_success' => 'Selected FAQ(s) have been activated successfully.',
    'inactive_success' => 'Selected FAQ(s) have been inactivated successfully.',
    'delete_success' => 'Selected FAQ(s) have been deleted successfully.',
    'display_order' => 'Display Order',
    'record_inactive_success' => 'The FAQ successfully inactivated.',
    'record_active_success' => 'The FAQ successfully activated.',
];
