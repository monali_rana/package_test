<?php


namespace Faq;

use Faq\Faq;
//use App\Events\BulkAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $faqs = new Faq();
        $faqs = $faqs->getResult($request);

        // Render view
        return view('faq::admin.modules.faq.index')->with('faqs', $faqs);
    }

    public function frontindex(Request $request)
    {
        $faqs = new Faq();
        $request['status'] = 1;
        $faqs = $faqs->getResult($request);
        // Render view
        return view('faq::front.modules.faq.index')->with('faqs', $faqs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Faq $faq)
    {
        $maxNumber = Faq::count();
        return view('faq::admin.modules.faq.addedit')->with(['faq'=> $faq,'maxnumber'=>$maxNumber])  ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Faq $faq)
    {
        $request = $this->stripHtmlTags($request, Faq::$notStripTags);
        $this->validation($request);

        $data = $request->all();

        // Save the faq Data
        $faq = new Faq();
        $faq->fill($data);
        $faq->save();

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('faq.edit', ['id' => $faq->id])->with("success", __('faq.create_success', ['title'=>$request->get('question')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('faq.index')->with("success", __('faq.create_success', ['title'=>$request->get('question')]));
        } else {
            return redirect()->route('faq.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        $maxNumber = Faq::count();
        return view('faq::admin.modules.faq.addedit')->with(['faq'=> $faq,'maxnumber'=>$maxNumber]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $request = $this->stripHtmlTags($request, Faq::$notStripTags);
        $this->validation($request);

        $oldPosition = $faq->display_order;

        // Prepare Data Array
        $data = $request->all();
        $newPosition = $data['display_order'];
        $data['display_order'] = $oldPosition;

        // Save the Data
        $faq->fill($data);
        $faq->save();

        //update sortorder
        $faqs = new Faq();
        $faqs->updateSortOrder($oldPosition, $newPosition, $faq->id);

        if ($request->get('btnsave') == 'savecontinue') {
            return redirect()->route('faq.edit', ['id' => $faq->id])->with("success", __('faq::faq.update_success', ['title'=>$request->get('question')]));
        } elseif ($request->get('btnsave') == 'save') {
            return redirect()->route('faq.index')->with("success", __('faq::faq.update_success', ['title'=>$request->get('question')]));
        } else {
            return redirect()->route('faq.index');
        }
    }

    /**
     * Validate the Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validation($request)
    {
        $rules = [
            'question' => 'required',
            'answer' => 'required',
            'status' => 'required'
        ];
        $this->validate($request, $rules);
    }

    /**
     * Apply bulk action on selected items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkaction(Request $request)
    {
        $faq = new Faq();

        if ($request->get('bulk-action') == 'delete') {
            Faq::destroy($request->get('id'));
            $message = __('faq::faq.delete_success');
        } elseif ($request->get('bulk-action') == 'active') {
            Faq::whereIn('id', $request->get('id'))->update(['status' => 1]);
            $message = __('faq::faq.active_success');
        } elseif ($request->get('bulk-action') == 'inactive') {
            Faq::whereIn('id', $request->get('id'))->update(['status' => 0]);
            $message = __('faq::faq.inactive_success');
        }
        $faq::flushCache($faq);
        return redirect()->back()->with('success', $message);
    }

    /**
     * Update display order of the items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $faq = new Faq();
        foreach ($request->get('display_order') as $orders) {
            $sort_faq = Faq::find($orders['0']);
            $sort_faq->fill(['display_order' => $orders['1']]);
            $sort_faq->save();
        }
        $faq::flushCache($faq);
        return response()->json(['success' => __('faq::faq.display_order_success')]);
    }

    /**
    * Apply change status
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function changestatus(Request $request)
    {
        $faq = Faq::findOrFail($request->id);
        $faq->status = $faq->status == 1?0:1;
        $faq->save();
        $message = displayMessages('faq::faq', $faq->status);
        return redirect()->back()->with('success', $message);
    }
}
