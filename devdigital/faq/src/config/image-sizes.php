<?php
/*
|--------------------------------------------------------------------------
| Default images which will generate while image upload
|--------------------------------------------------------------------------
|
| This option contains all available resized images
|
*/

use Spatie\Image\Manipulations;

return [
    'team' => [
        'fit-300x410' => [
            'width' => 300,
            'height' => 410,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN,
            'is_recommended' => true
        ],
        'fit-150x210' => [
            'width' => 150,
            'height' => 210,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN
        ],
        'fit-45x45' => [
            'width' => 45,
            'height' => 45,
            'method' => 'fit',
            'size' => 5,
            'type' => Manipulations::FIT_CONTAIN
        ]
    ],
];
