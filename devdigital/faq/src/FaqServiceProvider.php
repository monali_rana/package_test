<?php

namespace Faq;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class FaqServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        include __DIR__.'/routes.php';
        $this->app->bind('Faq\FaqController');
        /* $this->mergeConfigFrom(
            __DIR__.'/config/image-sizes.php', 'image-sizes'
        );
      */
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views','faq');
        $this->loadTranslationsFrom($this->app->basePath(). '/packages/devdigital/faq/src/lang','faq');
        View::composer("faq::front.modules.faq.index","Faq\ViewComposers\Front\FaqComposer");
        View::composer("faq::front.modules.section.faq","Faq\ViewComposers\Front\FaqComposer");
    }

 
}
