<?php

namespace Faq\ViewComposers\Front;

use Cache;
use Faq\Faq;
use Illuminate\Contracts\View\View;

class FaqComposer {

    /*
    * Compose Method to Pass Slider Data in View
    */
    public function compose(View $view) {
        $faqs =  Cache::rememberForever('faq-composer', function() {
            return Faq::where('status',1)->orderBy('display_order','ASC')->get();
        });
        
        $view->with('faqs',$faqs);
    }
}